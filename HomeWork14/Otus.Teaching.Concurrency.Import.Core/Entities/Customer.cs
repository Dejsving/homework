﻿using System;
using System.Xml;

namespace Otus.Teaching.Concurrency.Import.Core.Entities
{
    public class Customer
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public Customer() { }

        public Customer(XmlNode node)
        {
            Id = int.Parse(node.ChildNodes[0].InnerText);
            FullName = node.ChildNodes[1].InnerText;
            Email = node.ChildNodes[2].InnerText;
            Phone = node.ChildNodes[3].InnerText;
        }

        public override string ToString()
        {
            return $"{Id} {FullName} {Email} {Phone}";
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
                throw new ArgumentNullException();
            Customer cObj = obj as Customer;
            if (cObj == null)
                throw new ArgumentException($"Невозможно преобразовать к типу {GetType()}");
            return Id.CompareTo(cObj.Id);
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            Customer cObj = obj as Customer;
            if (cObj == null)
                return false;
            return Id == cObj.Id &&
                   FullName == cObj.FullName &&
                   Email == cObj.Email &&
                   Phone == cObj.Phone;
        }
        public static bool operator ==(Customer c1, Customer c2)
        {
            if ((object)c1 == null)
                return false;
            return c1.Equals(c2);
        }
        public static bool operator !=(Customer c1, Customer c2)
        {
            return !(c1 == c2);
        }
    }
}