﻿using Otus.Teaching.Concurrency.Import.Core.Generators;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;
using CsvDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.CsvGenerator;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.App
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetXmlGenerator(string fileName, int dataCount)
        {
            return new XmlDataGenerator(fileName, dataCount);
        }

        public static IDataGenerator GetCsvGenerator(string fileName, int dataCount)
        {
            return new CsvDataGenerator(fileName, dataCount);
        }
    }
}