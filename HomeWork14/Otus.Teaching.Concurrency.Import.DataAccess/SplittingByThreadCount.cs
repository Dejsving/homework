﻿using System;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    /// <summary>
    /// Класс для настройки кол-ва потоков
    /// </summary>
    public class SplittingByThreadCount
    {
        /// <summary>
        /// Кол-во желаемых потоков (по умолчанию - кол-во ядер процессора)
        /// </summary>
        public int ThreadCount { get; }
        /// <summary>
        /// Общее кол-во элементов в коллекции
        /// </summary>
        public int TotalCount { get; }
        /// <summary>
        /// По сколько элементов требуется обрабатывать
        /// </summary>
        public int Step { get; }
        /// <summary>
        /// Остаточное кол-во элементов - для обработки в основном потоке (будет меньше кол-ва процессоров)
        /// </summary>
        public int Remainder { get; }

        public SplittingByThreadCount(int totalCount, int threadCount = 0)
        {
            ThreadCount = threadCount == 0 ? Environment.ProcessorCount : threadCount;
            TotalCount = totalCount;
            Step = TotalCount / ThreadCount;
            Remainder = TotalCount - ThreadCount * Step;
        }
    }
}