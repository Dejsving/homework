﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Xml;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Parsers;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<List<Customer>>
    {
        delegate List<Customer> GetParthCustomersDelegate(XmlNodeList xmlCustomers, int step, int threadNumber);
        SplittingByThreadCount Splitting { get; }
        XmlNodeList XmlCustomers { get; }

        public XmlParser(XmlNodeList xmlCustomers, int threadCount = 0)
        {
            Splitting = new SplittingByThreadCount(xmlCustomers.Count, threadCount);
            XmlCustomers = xmlCustomers;
        }

        public List<Customer> Parse()
        {
            List<Customer> result = new List<Customer>();
            var customersCount = XmlCustomers.Count;
            
            List<Customer>[] customersLists = new List<Customer>[Splitting.ThreadCount];
            GetParthCustomersDelegate getParthCustomersDelegate = GetParthCustomers;
            
            /*
            var asyncResults = new List<IAsyncResult>();

            for (int i = 0; i < Splitting.ThreadCount; i++)
                asyncResults.Add(getParthCustomersDelegate.BeginInvoke(
                    XmlCustomers, Splitting.Step, i, null, null));

            for (int i = 0; i < Splitting.Remainder; i++)
            {
                result.Add(new Customer(XmlCustomers[customersCount - i - 1]));
            }

            foreach (var asyncResult in asyncResults)
            {
                if (!asyncResult.CompletedSynchronously)
                {
                    asyncResult.AsyncWaitHandle.WaitOne();
                }
                result.AddRange(getParthCustomersDelegate.EndInvoke(asyncResult));
            }
            */

            var tasks = new List<Task<List<Customer>>>();
            for (int i = 0; i < Splitting.ThreadCount; i++)
            {
                var threadNumber = i;
                tasks.Add(new Task<List<Customer>>(() =>
                    GetParthCustomers(XmlCustomers, Splitting.Step, threadNumber)));
                Console.WriteLine($"GetParthCustomers({XmlCustomers}, {Splitting.Step}, {i});");
            }

            tasks.ForEach(t => t.Start());

            for (int i = 0; i < Splitting.Remainder; i++)
            {
                result.Add(new Customer(XmlCustomers[customersCount - i - 1]));
            }

            tasks.ForEach(t => t.Wait());

            foreach (var task in tasks)
            {
                result.AddRange(task.Result);
            }

            //for (int i = 0; i < Splitting.ThreadCount; i++)
            //{
            //    result.AddRange(GetParthCustomers(XmlCustomers, Splitting.Step, i));
            //    Console.WriteLine($"GetParthCustomers({XmlCustomers}, {Splitting.Step}, {i});");
            //}

            //for (int i = 0; i < Splitting.Remainder; i++)
            //{
            //    result.Add(new Customer(XmlCustomers[customersCount - i - 1]));
            //}

            return result;
        }

        private List<Customer> GetParthCustomers(XmlNodeList xmlCustomers, int step, int threadNumber)
        {
            var result = new List<Customer>();

            for (int j = 0; j < step; j++)
            {
                result.Add(new Customer(xmlCustomers[j + step * threadNumber]));
            }

            Console.WriteLine($"Task {threadNumber} completed");
            return result;
        }
    }
}