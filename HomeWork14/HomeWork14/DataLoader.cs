﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess;

namespace HomeWork14
{
    public class DataLoader : IDataLoader
    {
        private List<Customer> Customers { get; }
        public string ConnectionString { get; set; }

        public DataLoader(List<Customer> customers, string connectionString)
        {
            Customers = customers;
            ConnectionString = connectionString;
        }

        public void LoadData()
        {
            using var connection = new SqlConnection(ConnectionString);
            
            var commands = new List<SqlCommand>();

            var splitting =
                new SplittingByThreadCount(Customers.Count, Customers.Count / 1000 + 1);

            for (int i = 0; i < splitting.ThreadCount; i++)
                commands.Add(CreateAddCustomerCommand(connection, splitting.Step, i));

            if (splitting.Remainder != 0)
                commands.Add(
                    CreateAddCustomerCommandRemainder(connection, Customers.Count, splitting.Remainder));

            connection.Open();
            var delCommand = new SqlCommand("delete from dbo.Customers", connection);
            delCommand.ExecuteNonQuery();
            int number = 0;
            foreach (var command in commands)
                number += command.ExecuteNonQuery();
            connection.Close();

            Console.WriteLine($"Загружено {number} записей");
        }

        private SqlCommand CreateAddCustomerCommand(SqlConnection connection, int step, int threadNumber)
        {
            StringBuilder sb = new StringBuilder("INSERT INTO dbo.Customers (Id, FullName,Email, Phone) VALUES ");
            
            for (int j = 0; j < step; j++)
            {
                var customer = Customers[j + step * threadNumber];
                sb.Append($"({customer.Id},");
                sb.Append($"'{customer.FullName.Replace("'", "''")}',");
                sb.Append($"'{customer.Email}',");
                sb.Append($"'{customer.Phone}'),");
            }
            //customersCount - i - 1 = j + step * threadNumber
            sb.Remove(sb.Length - 1, 1);
            string sqlExpression = sb.ToString();
            return new SqlCommand(sqlExpression, connection);
        }

        private SqlCommand CreateAddCustomerCommandRemainder(SqlConnection connection, int customersCount, int remainder)
        {
            StringBuilder sb = new StringBuilder("INSERT INTO dbo.Customers (Id, FullName,Email, Phone) VALUES ");

            for (int i = 0; i < remainder; i++)
            {
                var customer = Customers[customersCount - i - 1];
                sb.Append($"({customer.Id},");
                sb.Append($"'{customer.FullName.Replace("'", "''")}',");
                sb.Append($"'{customer.Email}',");
                sb.Append($"'{customer.Phone}'),");
            }

            sb.Remove(sb.Length - 1, 1);
            string sqlExpression = sb.ToString();
            return new SqlCommand(sqlExpression, connection);
        }
    }
}