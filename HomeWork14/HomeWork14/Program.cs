﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Xml;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.App;

namespace HomeWork14
{
    class Program
    {
        static void Main(string[] args)
        {
            using var Console = new StreamWriter("results.txt");

            var sw = new Stopwatch();

            sw.Start();
            GenerateFile("Clients", 1_000_000, args);
            sw.Stop();
            Console.WriteLine($"Генерация: {sw.ElapsedMilliseconds/1000.0} c");

            sw.Restart();
            var parser = new XmlParser(LoadFile("Clients.xml"), 0);
            var customers = parser.Parse();
            sw.Stop();
            Console.WriteLine($"Парсинг: {sw.ElapsedMilliseconds / 1000.0} c");

            sw.Restart();
            var loader = new DataLoader(customers, GetConnectionString());
            loader.LoadData();
            sw.Stop();
            Console.WriteLine($"Загрузка данных: {sw.ElapsedMilliseconds / 1000.0} c");
        }

        #region GenerateFile
        static void GenerateFile(string fileName, int count, string[] args)
        {
            if (args != null && args.Length > 0 &&
                string.Compare(args[0], "file", StringComparison.CurrentCultureIgnoreCase) == 0)
                GenerateFileByProcess(fileName, count);
            else
                GenerateFileByFunction(fileName, count);
        }

        static void GenerateFileByProcess(string fileName, int count)
        {
            var startInfo = new ProcessStartInfo
            {
                FileName = "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe",
                WindowStyle = ProcessWindowStyle.Hidden,
                Arguments = $"{fileName} {count}"
            };
            Process.Start(startInfo);
        }

        static void GenerateFileByFunction(string fileName, int count)
        {
            var generator = GeneratorFactory.GetXmlGenerator(fileName + ".xml", count);
            generator.Generate();
        }
        #endregion

        static string GetConnectionString()
        {
            var appSettings = ConfigurationManager.AppSettings;
            var connectionString = appSettings["connectionString"];
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException();
            return connectionString;
        }

        static XmlNodeList LoadFile(string filePath)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(filePath);
            var xRoot = xDoc.DocumentElement;
            return xRoot?.FirstChild.ChildNodes;
        }
    }
}
