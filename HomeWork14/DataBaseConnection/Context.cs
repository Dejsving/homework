﻿using System.Data.Entity;
using DataBaseConnection.Configuration;
using DataBaseConnection.Core;

namespace DataBaseConnection
{
    public class Context : DbContext
    {
        public Context(string connectionString) : base(connectionString) { }

        public DbSet<Customer> Teachers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CustomerConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}