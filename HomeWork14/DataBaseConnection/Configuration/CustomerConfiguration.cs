﻿using System.Data.Entity.ModelConfiguration;
using DataBaseConnection.Core;

namespace DataBaseConnection.Configuration
{
    public class CustomerConfiguration : EntityTypeConfiguration<Customer>
    {
        public CustomerConfiguration()
        {
            ToTable("Customer", "otus");
            HasKey(t => t.Id);
        }
    }
}