﻿using System.IO;
using System.Text;
using Otus.Teaching.Concurrency.Import.Core.Generators;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;

        public CsvGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            using var stream = File.Create(_fileName);
            Serialize(stream, new CustomersList()
            {
                Customers = customers
            });
        }

        private void Serialize(FileStream stream, CustomersList customersList)
        {
            StringBuilder serializeList = new StringBuilder();

            foreach (var customer in customersList.Customers)
            {
                serializeList.Append(customer.Id + ";");
                serializeList.Append(customer.FullName + ";");
                serializeList.Append(customer.Email + ";");
                serializeList.AppendLine(customer.Phone);
            }

            using StreamWriter file = new StreamWriter(stream);
            file.WriteLine(serializeList);
        }
    }
}