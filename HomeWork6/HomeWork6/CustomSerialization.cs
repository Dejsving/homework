﻿using System.Collections.Generic;
using System.Text;

namespace HomeWork.Reflection
{
    public static class CustomSerialization
    {
        public static string ToCsv(object o)
        {
            var type = o.GetType();
            var fields = type.GetProperties();
            var result = new StringBuilder("{");
            foreach (var f in fields)
            {
                result.Append($"\"{f.Name}\":{f.GetValue(o)},");
            }
            result.Remove(result.Length - 1, 1);
            result.Append("}");
            return result.ToString();
        }

        public static T FromCsv<T>(string input) where T : new()
        {
            T result = new T();
            var type = typeof(T);
            var values = new Dictionary<string, int>();
            input = input.Trim('{').Trim('}');
            foreach (var value in input.Split(','))
            {
                var trimValue = value.Trim('\"');
                var valueStringArr = trimValue.Split(':');
                var valueKey = valueStringArr[0].Trim('\"');
                if (int.TryParse(valueStringArr[1], out var i))
                    values.Add(valueKey, i);
            }

            var props = type.GetProperties();
            foreach (var p in props)
            {
                var propertyInfo = type.GetProperty(p.Name);
                propertyInfo.SetValue(result, values[p.Name]);
            }
            return result;
        }
    }
}
