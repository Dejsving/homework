﻿namespace HomeWork.Reflection
{
    class ReflectionClass
    {
        public int int1 { get; set; }
        public int int2 { get; set; }
        public int int3 { get; set; }
        public int int4 { get; set; }
        public int int5 { get; set; }

        public static ReflectionClass Get() => new ReflectionClass()
        {
            int1 = 1,
            int2 = 2,
            int3 = 3,
            int4 = 4,
            int5 = 5
        };
    }
}
