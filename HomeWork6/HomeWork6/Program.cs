﻿using HomeWork.Reflection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using static HomeWork.Reflection.CustomSerialization;
using static Newtonsoft.Json.JsonConvert;
using Generic = System.Collections.Generic;

namespace Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            CheckReflection();
            Console.ReadKey();
        }

        static void CheckReflection()
        {
            var ef = new double[2];

            Console.WriteLine("Custom");
            Console.WriteLine("Сериализация");
            var reflectionClass = ReflectionClass.Get();
            var type = reflectionClass.GetType();
            type.GetProperties();

            var SerializeList = new List<string>(10000);
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < 1_000_000; i++)
            {
                SerializeList.Add(ToCsv(ReflectionClass.Get()));
            }
            sw.Stop();
            var custom = sw.ElapsedMilliseconds;
            ef[0] = (double)custom;
            Console.WriteLine(SerializeList[0]);
            Console.WriteLine("Сериализация");
            Console.WriteLine($"Время на сериализацию 1 объекта {(double)custom / 1_000_000} мс");
            var ReflectionClassList = new Generic.List<ReflectionClass>(10000);
            sw.Reset();
            sw.Start();
            foreach (var refString in SerializeList)
            {
                ReflectionClassList.Add(FromCsv<ReflectionClass>(refString));
            }
            sw.Stop();
            custom = sw.ElapsedMilliseconds;
            ef[1] = (double)custom;
            Console.WriteLine("Десериализация");
            Console.WriteLine($"Время на десериализацию 1 объекта {(double)custom / 1_000_000} мс");
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("Newtonsoft");
            ReflectionClassList.Clear();
            SerializeList.Clear();
            sw.Reset();
            sw.Start();
            for (int i = 0; i < 1_000_000; i++)
            {
                SerializeList.Add(SerializeObject(ReflectionClass.Get()));
            }
            sw.Stop();
            var newtonsoft = sw.ElapsedMilliseconds;
            ef[0] /= newtonsoft;
            Console.WriteLine(SerializeList[0]);
            Console.WriteLine("Сериализация");
            Console.WriteLine($"Время на сериализацию 1 объекта {(double)newtonsoft / 1_000_000} мс");
            sw.Reset();
            sw.Start();
            foreach (var refString in SerializeList)
            {
                ReflectionClassList.Add(DeserializeObject<ReflectionClass>(refString));
            }
            sw.Stop();
            newtonsoft = sw.ElapsedMilliseconds;
            ef[1] /= newtonsoft;
            Console.WriteLine("Десериализация");
            Console.WriteLine($"Время на десериализацию 1 объекта {(double)newtonsoft / 1_000_000} мс");
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine($"Newtonsoft быстрее Custom:");
            Console.WriteLine($"{ef[0] / 100}% при сериализации");
            Console.WriteLine($"{ef[1] / 100}% при десериализации");
            Console.WriteLine();
        }
    }
}
