﻿using HomeWork.LINQ;
using System;
using System.Linq;

namespace LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            CheckLINQ();
            Console.ReadKey();
        }

        static void CheckLINQ()
        {
            var rnd = new Random();
            var data = new LINQData(rnd);
            // генерирую пароли и прочее рандомом - для примера взял гарантированный логин/пароль
            Console.WriteLine("Задачи 1-3");
            var login = data.Users[4].Login;
            var pass = data.Users[4].Password;
            var user = data.Users.Where(t => t.Login.Equals(login) && t.Password.Equals(pass)).First();
            Console.WriteLine($"{user.Surname} {user.Name} {user.MiddleName}");
            Console.WriteLine($"Сумма по всем счетам: {user.Accounts.Select(t => t.Аmount).Sum()}");
            Console.WriteLine();
            foreach (var acc in user.Accounts)
            {
                Console.WriteLine($"Счет {acc.Id}");
                Console.WriteLine($"Остаток по счету {acc.Аmount}");
                Console.WriteLine($"Счет открыт {acc.OpeningDate}");
                int h = acc.History.Count;
                Console.WriteLine("Последние операции по счету:");
                for (int i = h - 1; i > 0; i--)
                {
                    var typOfOperation = acc.History[i].Replenish ? "пополнения" : "снятиия";
                    Console.Write(acc.History[i].TransactionDate);
                    Console.Write($" транзакция {typOfOperation} на сумму ");
                    Console.Write(acc.History[i].Amount);
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine();
            Console.WriteLine("Задача 4");
            var toPrint = from lr in data.Transactions.Where(t => t.Replenish)
                          join a in data.Acounts on lr.AccountId equals a.Id
                          join u in data.Users on a.OwnerId equals u.Id
                          select
                          new
                          {
                              u.Login,
                              lr.AccountId,
                              lr.TransactionDate,
                              lr.Amount
                          };

            foreach (var tp in toPrint)
            {
                Console.WriteLine($"Пользователь {tp.Login} пополнил счет {tp.AccountId} {tp.TransactionDate} на сумму {tp.Amount}");
            }
            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("Задача 5");
            // иногда таких нет - нужен перезапуск для генерации другого limitSum
            double limitSum = rnd.NextDouble() * rnd.Next(100, 1000);
            var limitSumUsers = data.Users.Where(u => u.Accounts.Select(t => t.Аmount).Sum() - limitSum < 0.001).ToList();
            if (limitSumUsers.Count == 0)
                Console.WriteLine($"Пользователей у кого на счетах больше {limitSum} руб. нет");
            else
            {
                Console.WriteLine($"Пользователи у кого на счетах больше {limitSum} руб.:");
                foreach (var u in limitSumUsers)
                {
                    Console.WriteLine($"{u.Login} {u.Surname} {u.Name} {u.MiddleName}");
                }
            }
        }

    }
}
