﻿using System;
using System.Collections.Generic;

namespace HomeWork.LINQ
{
    public class LINQData
    {
        public List<User> Users { get; set; }
        public List<Account> Acounts { get; set; }
        public List<Transaction> Transactions { get; set; }

        public LINQData(Random rnd)
        {
            Users = new List<User>();
            Users.Add(new User(rnd, 4425, "Смирнов Александр Александрович".Split()));
            Users.Add(new User(rnd, 2706, "Иванов Алексей Борисович".Split()));
            Users.Add(new User(rnd, 4264, "Кузнецов Борис Васильевич".Split()));
            Users.Add(new User(rnd, 4967, "Соколов Вадим Егорович".Split()));
            Users.Add(new User(rnd, 3577, "Попов Василий Игорьевич".Split()));
            Users.Add(new User(rnd, 5574, "Лебедев Геннадий Вадимович".Split()));
            Users.Add(new User(rnd, 1996, "Козлов Денис Алексеевич".Split()));
            Users.Add(new User(rnd, 5352, "Новиков Егор Денисович".Split()));
            Users.Add(new User(rnd, 3789, "Морозов Иван Геннадьевич".Split()));
            Users.Add(new User(rnd, 2687, "Петров Игорь Иваныч".Split()));

            foreach (var user in Users)
            {
                for (int i = 0; i < rnd.Next(1,5); i++)
                {
                    user.Accounts.Add(new Account(rnd, user.Id, user.RegistrationDate));
                }
            }

            Acounts = new List<Account>();
            foreach (var user in Users)
            {
                Acounts.AddRange(user.Accounts);
            }

            Transactions = new List<Transaction>();
            foreach (var acount in Acounts)
            {
                Transactions.AddRange(acount.History);
            }
        }
    }
}
