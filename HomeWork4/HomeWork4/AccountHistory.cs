﻿using System;

namespace HomeWork.LINQ
{
    /// <summary>
    /// История по счету
    /// </summary>
    public class Transaction
    {
        public int Id { get; set; }
        /// <summary>
        /// Счет
        /// </summary>
        public string AccountId { get; set; }
        /// <summary>
        /// Дата открытия счета
        /// </summary>
        public DateTime TransactionDate { get; set; }
        /// <summary>
        /// Операция пополнения или нет
        /// </summary>
        public bool Replenish { get; set; }
        /// <summary>
        /// Сумма
        /// </summary>
        public double Amount { get; set; }

        public Transaction(Random rnd, string id, DateTime openingDate, double amount)
        {
            AccountId = id;

            do
            {
                TransactionDate = new DateTime(rnd.Next(1900, 2000), rnd.Next(1, 12), rnd.Next(1, 28));
            } while (TransactionDate < openingDate);

            Replenish = rnd.Next(0, 1000) > 500;

            Amount = Math.Round(rnd.NextDouble() * 100, 2);

            if (!Replenish && Amount > amount)
                Amount = amount;
        }
    }
}
