﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HomeWork.LINQ
{
    /// <summary>
    /// Счет
    /// </summary>
    public class Account
    {
        public string Id { get; set; }
        /// <summary>
        /// Владелец
        /// </summary>
        public int OwnerId { get; set; }
        /// <summary>
        /// Сумма на счете
        /// </summary>
        public double Аmount { get; set; }
        /// <summary>
        /// Дата открытия счета
        /// </summary>
        public DateTime OpeningDate { get; set; }
        /// <summary>
        /// История операций
        /// </summary>
        public List<Transaction> History { get; set; }

        public Account(Random rnd, int OwnerId, DateTime registrationDate)
        {
            this.OwnerId = OwnerId;
            do
            {
                OpeningDate = new DateTime(rnd.Next(1900, 2000), rnd.Next(1, 12), rnd.Next(1, 28));
            } while (OpeningDate < registrationDate);
            Аmount = Math.Round(rnd.NextDouble() * 1000, 2);

            var id = new byte[16];
            rnd.NextBytes(id);
            Id = string.Join("", id.Select(t => t % 10));

            History = new List<Transaction>();
            for (int i = 0; i < rnd.Next(1, 100); i++)
            {
                var transaction = new Transaction(rnd, Id, OpeningDate, Аmount);
                History.Add(transaction);
                Аmount += transaction.Replenish ? transaction.Amount : -transaction.Amount;
            }
        }
    }
}
