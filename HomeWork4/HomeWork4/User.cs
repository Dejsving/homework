﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HomeWork.LINQ
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User
    {
        public int Id { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string Surname { get; set; }
        /// <summary>
        /// Отчесвто
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Номер телефона, строго 10 цифр
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Серия и номер паспорта, строго 10 цифр
        /// </summary>
        public string Passport { get; set; }
        /// <summary>
        /// Дата регистрации в системе
        /// </summary>
        public DateTime RegistrationDate { get; set; }
        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Пароль
        /// </summary>
        /// <remarks>
        /// Решил хранить в открытом виде
        /// Будем считать, что мне приходит уже хэш
        /// А то велосипед как-то у меня не изобретается
        /// </remarks>
        public string Password { get; set; }
        /// <summary>
        /// Счета пользователя
        /// </summary>
        public List<Account> Accounts { get; set; }

        public User(Random rnd, int id, string[] fio)
        {
            Id = id;
            Surname = fio[0];
            Name = fio[1];
            MiddleName = fio[2];

            var phone = new byte[10];
            rnd.NextBytes(phone);
            Phone = string.Join("", phone.Select(t => t % 10));

            var passport = new byte[10];
            rnd.NextBytes(passport);
            Passport = string.Join("", passport.Select(t => t % 10));

            RegistrationDate = new DateTime(rnd.Next(1900, 2000), rnd.Next(1, 12), rnd.Next(1, 28));

            var login = new byte[10];
            rnd.NextBytes(login);
            Login = string.Join("", login.Select(t => char.ConvertFromUtf32(t % 25 + 65))).ToLower();

            var password = new byte[20];
            rnd.NextBytes(password);
            Password = string.Join("", password.Select(t => char.ConvertFromUtf32(t % 25 + 65))).ToLower();

            Accounts = new List<Account>();
        }
    }
}
