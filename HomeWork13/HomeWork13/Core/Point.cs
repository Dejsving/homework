﻿using System;

namespace HomeWork13.Core
{
    public class Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point()
        {
            var random = new Random();
            X = random.Next(1, 5) + random.NextDouble();
            Y = random.Next(1, 5) + random.NextDouble();
        }
    }
}