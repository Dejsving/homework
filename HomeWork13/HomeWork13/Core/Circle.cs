﻿using System;

namespace HomeWork13.Core
{
    public class Circle
    {
        public double CenterX { get; set; }
        public double CenterY { get; set; }
        public double Radius { get; set; }

        public Circle()
        {
            var random = new Random();
            CenterX = random.Next(1, 5) + random.NextDouble();
            CenterY = random.Next(1, 5) + random.NextDouble();
            Radius = random.Next(1, 5) + random.NextDouble();
        }
    }
}