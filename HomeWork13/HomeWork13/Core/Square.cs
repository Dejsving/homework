﻿using System;

namespace HomeWork13.Core
{
    public class Square
    {
        public double CenterX { get; set; }
        public double CenterY { get; set; }
        public double SideLength { get; set; }

        public Square()
        {
            var random = new Random();
            CenterX = random.Next(1, 5) + random.NextDouble();
            CenterY = random.Next(1, 5) + random.NextDouble();
            SideLength = random.Next(1, 5) + random.NextDouble();
        }
    }
}