﻿using HomeWork13.Strategies;
using HomeWork13.Visitors;

namespace HomeWork13
{
    public class Context
    {
        private IVisitor Visitor { get; }
        private IStrategy Strategy { get; }

        public Context(IStrategy strategy, IVisitor visitor)
        {
            Strategy = strategy;
            Visitor = visitor;
        }

        public string Execute()
        {
            return Strategy.Execute(Visitor);
        }
    }
}