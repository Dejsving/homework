﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeWork13.Core;
using HomeWork13.Strategies;
using HomeWork13.Visitors;

namespace HomeWork13
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IStrategy> strategies = new List<IStrategy>
            {
                new JsonStrategy(),
                new XmlStrategy()
            };

            List<IVisitor> visitors = new List<IVisitor>
            {
                new VisitorCircle(GenerateCircles()),
                new VisitorPoint(GeneratePoints()),
                new VisitorSquare(GenerateSquares())
            };

            // проверим все стратегии и всех посетителей
            foreach (var strategy in strategies)
                foreach (var visitor in visitors)
                    CheckStrategyVisitor(strategy, visitor);
        }

        /// <summary>
        /// Проверка работы конкретной стратегии и конкретного посетителя
        /// </summary>
        /// <param name="strategy">Стратегия для проверки</param>
        /// <param name="visitor">Посетитель для проверки</param>
        private static void CheckStrategyVisitor(IStrategy strategy, IVisitor visitor)
        {
            string s = strategy.GetType().ToString().Split('.').Last()
                .ToLower().Replace("strategy", "");
            string v = visitor.GetType().ToString().Split('.').Last()
                .ToLower().Replace("visitor", "");
            using var file = new StreamWriter($"{v}.{s}");
            Context context = new Context(strategy, visitor);
            file.Write(context.Execute());
        }

        private static List<Circle> GenerateCircles()
        {
            return new List<Circle>
            {
                new Circle(), new Circle(), new Circle(), new Circle(), new Circle()
            };
        }
        private static List<Point> GeneratePoints()
        {
            return new List<Point>
            {
                new Point(), new Point(), new Point(), new Point(), new Point()
            };
        }
        private static List<Square> GenerateSquares()
        {
            return new List<Square>
            {
                new Square(), new Square(), new Square(), new Square(), new Square()
            };
        }
    }
}
