﻿using HomeWork13.Visitors;

namespace HomeWork13.Strategies
{
    public interface IStrategy
    {
        string Execute(IVisitor visitor);
    }
}