﻿using HomeWork13.Visitors;

namespace HomeWork13.Strategies
{
    public class XmlStrategy : IStrategy
    {
        public string Execute(IVisitor visitor)
        {
            return visitor.SerializeToXml();
        }
    }
}