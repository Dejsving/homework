﻿using HomeWork13.Visitors;

namespace HomeWork13.Strategies
{
    public class JsonStrategy : IStrategy
    {
        public string Execute(IVisitor visitor)
        {
            return visitor.SerializeToJson();
        }
    }
}
