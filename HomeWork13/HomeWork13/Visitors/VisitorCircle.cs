﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using HomeWork13.Core;
using HomeWork13.Visitors;

namespace HomeWork13
{
    public class VisitorCircle : IVisitor
    {
        private List<Circle> _circles;

        public VisitorCircle(List<Circle> circles)
        {
            _circles = circles;
        }

        public string SerializeToJson()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("{\"Circles\":\r\n[");
            foreach (var item in _circles)
                stringBuilder.AppendLine(SerializeObjectToJson(item));
            stringBuilder.AppendLine("]}");
            return stringBuilder.ToString();
        }

        public string SerializeToXml()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("<Circles>");
            foreach (var circle in _circles)
            {
                stringBuilder.AppendLine("<Circle>");
                stringBuilder.AppendLine(SerializeObjectToXml(circle));
                stringBuilder.AppendLine("</Circle>");
            }
            stringBuilder.Append("</Circles>");
            return stringBuilder.ToString();
        }

        private string SerializeObjectToJson(object obj)
        {
            var fields = obj.GetType().GetProperties().ToList();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("\t{");
            foreach (var field in fields)
                stringBuilder.AppendLine("\t\t\"" + field.Name + "\": \"" + field.GetValue(obj) + "\",");
            stringBuilder.Remove(stringBuilder.Length - 1, 1);
            stringBuilder.AppendLine("\t}");
            return stringBuilder.ToString();
        }

        private string SerializeObjectToXml(object obj)
        {
            var fields = obj.GetType().GetProperties().ToList();
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var field in fields)
                stringBuilder.AppendLine("<" + field.Name + ">" + field.GetValue(obj) + "</" + field.Name + ">");
            return stringBuilder.ToString();
        }
    }
}