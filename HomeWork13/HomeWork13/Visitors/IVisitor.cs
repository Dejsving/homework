﻿namespace HomeWork13.Visitors
{
    public interface IVisitor
    {
        string SerializeToJson();
        string SerializeToXml();
    }
}