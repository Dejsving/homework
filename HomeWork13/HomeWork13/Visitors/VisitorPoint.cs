﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using HomeWork13.Core;

namespace HomeWork13.Visitors
{
    public class VisitorPoint : IVisitor
    {
        private readonly List<Point> _points;

        public VisitorPoint(List<Point> points)
        {
            _points = points;
        }

        public string SerializeToJson()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("{\"Points\":\r\n[");
            foreach (var item in _points)
                stringBuilder.AppendLine(SerializeObjectToJson(item));
            stringBuilder.Append("]}");
            return stringBuilder.ToString();
        }

        public string SerializeToXml()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("<Points>");
            foreach (var circle in _points)
            {
                stringBuilder.AppendLine("<Point>");
                stringBuilder.AppendLine(SerializeObjectToXml(circle));
                stringBuilder.Append("</Point>");
            }
            stringBuilder.AppendLine("</Points>");
            return stringBuilder.ToString();
        }

        private string SerializeObjectToJson(object obj)
        {
            var fields = obj.GetType().GetProperties().ToList();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("\t{");
            foreach (var field in fields)
                stringBuilder.AppendLine("\t\t\"" + field.Name + "\": \"" + field.GetValue(obj) + "\",");
            stringBuilder.Remove(stringBuilder.Length - 1, 1);
            stringBuilder.AppendLine("\t}");
            return stringBuilder.ToString();
        }

        private string SerializeObjectToXml(object obj)
        {
            var fields = obj.GetType().GetProperties().ToList();
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var field in fields)
                stringBuilder.AppendLine("<" + field.Name + ">" + field.GetValue(obj) + "</" + field.Name + ">");
            return stringBuilder.ToString();
        }
    }
}
