﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using HomeWork13.Core;

namespace HomeWork13.Visitors
{
    public class VisitorSquare : IVisitor
    {
        private List<Square> _squares;

        public VisitorSquare(List<Square> squares)
        {
            _squares = squares;
        }

        public string SerializeToJson()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("{\"Squares\":\r\n[");
            foreach (var item in _squares)
                stringBuilder.AppendLine(SerializeObjectToJson(item));
            stringBuilder.Append("]}");
            return stringBuilder.ToString();
        }

        public string SerializeToXml()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("<Squares>");
            foreach (var circle in _squares)
            {
                stringBuilder.AppendLine("<Square>");
                stringBuilder.AppendLine(SerializeObjectToXml(circle));
                stringBuilder.Append("</Square>");
            }
            stringBuilder.AppendLine("</Squares>");
            return stringBuilder.ToString();
        }

        private string SerializeObjectToJson(object obj)
        {
            var fields = obj.GetType().GetProperties().ToList();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("\t{");
            foreach (var field in fields)
                stringBuilder.AppendLine("\t\t\"" + field.Name + "\": \"" + field.GetValue(obj) + "\",");
            stringBuilder.Remove(stringBuilder.Length - 1, 1);
            stringBuilder.AppendLine("\t}");
            return stringBuilder.ToString();
        }

        private string SerializeObjectToXml(object obj)
        {
            var fields = obj.GetType().GetProperties().ToList();
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var field in fields)
                stringBuilder.AppendLine("<" + field.Name + ">" + field.GetValue(obj) + "</" + field.Name + ">");
            return stringBuilder.ToString();
        }
    }
}
