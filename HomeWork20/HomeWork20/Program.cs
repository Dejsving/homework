﻿using System;
using HomeWork20.InputOutput;

namespace HomeWork20
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleData data = new ConsoleData();
            Model model = new Model(data, data);
            ICalculate calculate = GetCalculate.Get(data);
            Context context = new Context(model, calculate);
            context.Execute();

            data = new Console2Data();
            model = new Model(data, data);
            context = new Context(model, calculate);
            context.Execute();

            Console.ReadKey();
        }
    }
}