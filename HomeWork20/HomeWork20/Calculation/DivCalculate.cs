﻿namespace HomeWork20
{
    public class DivCalculate : ICalculate
    {
        private int A { get; set; }
        private int B { get; set; }

        public DivCalculate(int a, int b)
        {
            A = a;
            B = b;
        }

        public int Calc()
        {
            return A / B;
        }
    }
}