﻿namespace HomeWork20
{
    public class SumCalculate : ICalculate
    {
        private int A { get; set; }
        private int B { get; set; }

        public SumCalculate(int a, int b)
        {
            A = a;
            B = b;
        }

        public int Calc()
        {
            return A + B;
        }
    }
}