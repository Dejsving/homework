﻿using HomeWork20.Core;
using HomeWork20.InputOutput.Interfaces;

namespace HomeWork20
{
    public static class GetCalculate
    {
        public static ICalculate Get(IInput input)
        {
            ICalculate result;
            var inputValueA = input.GetNumber("Enter a: ");
            var inputValueB = input.GetNumber("Enter b: ");
            var operation = input.GetOperation("Enter operation: ");
            if (operation == Operation.sum)
                result = new SumCalculate(inputValueA, inputValueB);
            else if (operation == Operation.dif)
                result = new DifCalculate(inputValueA, inputValueB);
            else if (operation == Operation.mul)
                result = new MulCalculate(inputValueA, inputValueB);
            else
                result = new DivCalculate(inputValueA, inputValueB);
            return result;
        }
    }
}