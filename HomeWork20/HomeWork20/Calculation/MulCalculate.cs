﻿namespace HomeWork20
{
    public class MulCalculate : ICalculate
    {
        private int A { get; set; }
        private int B { get; set; }

        public MulCalculate(int a, int b)
        {
            A = a;
            B = b;
        }

        public int Calc()
        {
            return A * B;
        }
    }
}