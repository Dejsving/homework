﻿namespace HomeWork20
{
    public class DifCalculate : ICalculate
    {
        private int A { get; set; }
        private int B { get; set; }

        public DifCalculate(int a, int b)
        {
            A = a;
            B = b;
        }

        public int Calc()
        {
            return A - B;
        }
    }
}