﻿namespace HomeWork20.InputOutput.Interfaces
{
    public interface IOutput
    {
        void WriteText(string text);
        void WriteNumber(int number);
    }
}