﻿using HomeWork20.Core;

namespace HomeWork20.InputOutput.Interfaces
{
    public interface IInput
    {
        int ReadNumber();
        int GetNumber(string text);
        Operation ReadOperation();
        Operation GetOperation(string text);
    }
}