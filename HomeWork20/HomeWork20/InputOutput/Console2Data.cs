﻿using System;

namespace HomeWork20.InputOutput
{
    public class Console2Data : ConsoleData
    {
        public override void WriteNumber(int number)
        {
            Console.WriteLine(Convert.ToString(number, 2));
        }
    }
}