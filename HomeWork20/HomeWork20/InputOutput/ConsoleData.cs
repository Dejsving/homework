﻿using System;
using HomeWork20.Core;
using HomeWork20.InputOutput.Interfaces;

namespace HomeWork20.InputOutput
{
    public class ConsoleData : IInput, IOutput
    {
        public void WriteText(string text)
        {
            Console.Write(text);
        }

        public virtual void WriteNumber(int number)
        {
            Console.WriteLine(number);
        }

        private string ReadText()
        {
            return Console.ReadLine();
        }

        public int ReadNumber()
        {
            var strRes = ReadText();
            if (!int.TryParse(strRes, out var result))
                throw new ArgumentException("Переданное число невозможно преобразовать к типу int");
            return result;
        }

        public int GetNumber(string text)
        {
            WriteText(text);
            return ReadNumber();
        }

        public Operation ReadOperation()
        {
            Console.WriteLine("Сумма: 'sum', Разность: 'dif', Произведение: 'mul', Отношение: 'div'");
            var strRes = ReadText().ToLower();
            if (!Enum.TryParse(strRes, out Operation result))
                throw new ArgumentException("Переданное число невозможно преобразовать к типу Operation");
            return result;
        }

        public Operation GetOperation(string text)
        {
            WriteText(text);
            return ReadOperation();
        }

    }
}