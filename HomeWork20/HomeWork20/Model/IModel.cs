﻿namespace HomeWork20
{
    public interface IModel
    {
        void Execute(ICalculate calculate);
    }
}