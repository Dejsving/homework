﻿using HomeWork20.InputOutput.Interfaces;

namespace HomeWork20
{
    public class Model : IModel
    {
        private int Result { get; set; }
        private IInput Input { get; }
        private IOutput Output { get; }
        private ICalculate Calculate { get; set; }

        public Model(IInput input, IOutput output)
        {
            Input = input;
            Output = output;
        }

        public void Execute(ICalculate calculate)
        {
            Calculation(calculate);
            GetResult();
        }
        
        void Calculation(ICalculate calculate)
        {
            Result = calculate.Calc();
        }

        void GetResult()
        {
            Output.WriteText("Результат вычислений: ");
            Output.WriteNumber(Result);
        }
    }
}