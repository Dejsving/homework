﻿namespace HomeWork20
{
    public class Context
    {
        private IModel Model { get; }
        private ICalculate Calculate { get; }

        public Context(IModel model, ICalculate calculate)
        {
            Model = model;
            Calculate = calculate;
        }

        public void Execute()
        {
           Model.Execute(Calculate);
        }
    }
}