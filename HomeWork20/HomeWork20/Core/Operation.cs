﻿using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace HomeWork20.Core
{
    [SuppressMessage("ReSharper", "InconsistentNaming")] // так преобразовывать легче
    public enum Operation
    {
        [Description("Сумма")]
        sum,
        [Description("Разность")]
        dif,
        [Description("Произведение")]
        mul,
        [Description("Отношение")]
        div
    }
}