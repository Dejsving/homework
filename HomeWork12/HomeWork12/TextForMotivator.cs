﻿using System;
using System.Drawing;

namespace HomeWork12
{
    public class TextForMotivator : IDisposable
    {
        public Bitmap TextAsImage { get; }

        public TextForMotivator(string text, Color color, int width, int height)
        {
            TextAsImage = new Bitmap(width, height);
            using (Graphics graphics = Graphics.FromImage(TextAsImage))
            {
                Font font = new Font("Arial", 25, GraphicsUnit.Point);
                SolidBrush brush = new SolidBrush(color);

                var stringSize = graphics.MeasureString(text, font);
                PointF point = new PointF((TextAsImage.Width - stringSize.Width) / 2, 0);

                graphics.DrawString(text, font, brush, point);
            }
        }

        public void Dispose()
        {
            TextAsImage.Dispose();
        }
    }
}