﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace HomeWork12
{
    public class MotivatorFacade
    {
        private int BorderWidth { get; }
        private Color BorderColor { get; }

        private string Text { get; }
        private Color TextColor { get; }

        private string StartImagePath { get; }
        private string FinishImagePath { get; }

        public MotivatorFacade(string text, Color textColor,
            int borderWidth, Color borderColor,
            string startImagePath, string finishImagePath)
        {
            Text = text;
            TextColor = textColor;
            StartImagePath = startImagePath;
            FinishImagePath = finishImagePath;
            BorderWidth = borderWidth;
            BorderColor = borderColor;
        }

        public void CreateMotivator()
        {
            using (var image = new Bitmap(StartImagePath))
            {
                using (var text = new TextForMotivator(Text, TextColor, image.Width, BorderWidth))
                {
                    using (var motivator = new TextPictureMotivator(text.TextAsImage, image, BorderWidth, BorderColor))
                    {
                        motivator.Save(FinishImagePath);
                    }
                }
            }
        }
    }
}