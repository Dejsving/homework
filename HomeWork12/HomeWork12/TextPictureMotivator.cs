﻿using System.Drawing;
using System.Drawing.Imaging;

namespace HomeWork12
{
    public class TextPictureMotivator : PictureMotivator
    {
        public TextPictureMotivator(Bitmap imageText, Bitmap image, int borderWidth, Color borderColor)
            : base(image, borderWidth, borderColor)
        {
            using (Graphics graphics = Graphics.FromImage(Motivator))
            {
                graphics.DrawImage(imageText,
                    new Rectangle(borderWidth, image.Height + borderWidth * 2, imageText.Width, imageText.Height));
            }
        }

        public void Save(string finishImagePath)
        {
            Motivator.Save(finishImagePath, ImageFormat.Jpeg);
        }
    }
}