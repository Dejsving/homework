﻿using System.Drawing;

namespace HomeWork12
{
    public class PictureMotivator : BaseMotivator
    {
        public PictureMotivator(Bitmap image, int borderWidth, Color borderColor) : base(image.Size, borderWidth, borderColor)
        {
            using (Graphics graphics = Graphics.FromImage(Motivator))
            {
                graphics.DrawImage(image, new Rectangle(borderWidth, borderWidth, image.Width, image.Height));
            }
        }
    }
}