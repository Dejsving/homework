﻿using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;

namespace HomeWork12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Читаю настройки");
            var settings = ReadAllSettings();
            Console.WriteLine("Обрабатываю картинку");
            MotivatorFacade facade = new MotivatorFacade(settings[0], Color.White,
                50, Color.Black,
                settings[1], settings[2]);
            facade.CreateMotivator();
            Console.WriteLine("Выполнено");
            Thread.Sleep(1000);
        }

        static string[] ReadAllSettings()
        {
            var appSettings = ConfigurationManager.AppSettings;
            var settings = new string[3];

            settings[0] = appSettings["Text"];
            if (string.IsNullOrWhiteSpace(settings[0]))
                settings[0] = "Test text for TextAsImage";

            settings[1] = appSettings["StartImagePath"];
            if (string.IsNullOrWhiteSpace(settings[1]))
                settings[1] = "Start.jpg";

            settings[2] = appSettings["FinishImagePath"];
            if (string.IsNullOrWhiteSpace(settings[2]))
                settings[2] = "Finish.jpg";

            return settings;
        }
    }
}
