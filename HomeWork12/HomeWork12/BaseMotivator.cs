﻿using System;
using System.Drawing;

namespace HomeWork12
{
    public class BaseMotivator : IDisposable
    {
        public Bitmap Motivator { get; }

        public BaseMotivator(Size size, int borderWidth, Color borderColor)
        {
            Motivator = new Bitmap(size.Width + borderWidth * 2, size.Height + borderWidth * 4);
            
            using (Graphics graphics = Graphics.FromImage(Motivator))
            {
                graphics.Clear(borderColor);
            }
        }

        public void Dispose()
        {
            Motivator.Dispose();
        }
    }
}