﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace HomeWork.Lists
{
    /// <summary>
    /// Односвязный список
    /// </summary>
    /// <typeparam name="T">Тип хранимых элементов</typeparam>
    public class List<T> : IEnumerable<T>, IEnumerable, ICloneable
    {
        /// <summary>
        /// Первый элемент
        /// </summary>
        Node<T> head;
        /// <summary>
        /// Последний элемент
        /// </summary>
        Node<T> tail;

        /// <summary>
        /// Добавление элемента
        /// </summary>
        /// <param name="data">Данные для добавления</param>
        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);

            if (head == null)
                head = node;
            else
                tail.Next = node;
            tail = node;

            Count++;
        }

        /// <summary>
        /// Удаление элемента
        /// </summary>
        /// <param name="data">Данные для удаления</param>
        /// <returns>Успешно ли удаление</returns>
        public bool Remove(T data)
        {
            Node<T> current = head;
            Node<T> previous = null;

            while (current != null)
            {
                if (current.Data.Equals(data))
                {
                    // Если узел в середине или в конце
                    if (previous != null)
                    {
                        // убираем узел current, теперь previous ссылается не на current, а на current.Next
                        previous.Next = current.Next;

                        // Если current.Next не установлен, значит узел последний,
                        // изменяем переменную tail
                        if (current.Next == null)
                            tail = previous;
                    }
                    else
                    {
                        // если удаляется первый элемент
                        // переустанавливаем значение head
                        head = head.Next;

                        // если после удаления список пуст, сбрасываем tail
                        if (head == null)
                            tail = null;
                    }
                    Count--;
                    return true;
                }

                previous = current;
                current = current.Next;
            }
            return false;
        }

        /// <summary>
        /// Количество элементов в списке
        /// </summary>
        public int Count { get; private set; }
        public bool IsEmpty { get { return Count == 0; } }

        /// <summary>
        /// Очистка списка
        /// </summary>
        public void Clear()
        {
            // дальше обо всем позаботится GC
            head = null;
            tail = null;
            Count = 0;
        }

        /// <summary>
        /// Содержание элемента в списке
        /// </summary>
        /// <param name="data">Данные для поиска</param>
        /// <returns></returns>
        public bool Contains(T data)
        {
            Node<T> current = head;
            while (current != null)
            {
                if (current.Data.Equals(data))
                    return true;
                current = current.Next;
            }
            return false;
        }

        /// <summary>
        /// Добавление элемента в начало списка
        /// </summary>
        /// <param name="data">Данные для добавления</param>
        public void AddFirst(T data)
        {
            Node<T> node = new Node<T>(data);
            node.Next = head;
            head = node;
            if (Count == 0)
                tail = head;
            Count++;
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> current = head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        public IEnumerator GetEnumerator()
        {
            Node<T> current = head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        public object Clone()
        {
            var result = new DoublyList<T>();
            if (IsEmpty)
                return result;

            foreach (var elem in this)
                result.Add((T)elem);

            return result;
        }

        class Node<T>
        {
            public Node(T data)
            {
                Data = data;
            }

            public T Data { get; set; }
            public Node<T> Next { get; set; }
        }
    }
}