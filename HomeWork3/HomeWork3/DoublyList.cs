﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace HomeWork.Lists
{
    /// <summary>
    /// Дву связный список
    /// </summary>
    /// <typeparam name="T">Тип хранимых элементов</typeparam>
    public class DoublyList<T> : IEnumerable<T>, IEnumerable, ICloneable
    {
        /// <summary>
        /// Первый элемент
        /// </summary>
        Node<T> head;
        /// <summary>
        /// Последний элемент
        /// </summary>
        Node<T> tail;

        /// <summary>
        /// Добавление элемента
        /// </summary>
        /// <param name="data">Данные для добавления</param>
        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);

            if (head == null)
                head = node;
            else
            {
                tail.Next = node;
                node.Previous = tail;
            }
            tail = node;
            Count++;
        }

        /// <summary>
        /// Удаление элемента
        /// </summary>
        /// <param name="data">Данные для уделения</param>
        /// <returns>Успешно ли удаление</returns>
        public bool Remove(T data)
        {
            Node<T> current = head;

            // поиск удаляемого узла
            while (current != null)
            {
                if (current.Data.Equals(data))
                {
                    break;
                }
                current = current.Next;
            }
            if (current != null)
            {
                // если узел не последний
                if (current.Next != null)
                {
                    current.Next.Previous = current.Previous;
                }
                else
                {
                    // если последний, переустанавливаем tail
                    tail = current.Previous;
                }

                // если узел не первый
                if (current.Previous != null)
                {
                    current.Previous.Next = current.Next;
                }
                else
                {
                    // если первый, переустанавливаем head
                    head = current.Next;
                }
                Count--;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Количество элементов в списке
        /// </summary>
        public int Count { get; private set; }
        public bool IsEmpty { get { return Count == 0; } }

        /// <summary>
        /// Очистка списка
        /// </summary>
        public void Clear()
        {
            // дальше обо всем позаботится GC
            head = null;
            tail = null;
            Count = 0;
        }

        /// <summary>
        /// Содержание элемента в списке
        /// </summary>
        /// <param name="data">Данные для поиска</param>
        /// <returns></returns>
        public bool Contains(T data)
        {
            Node<T> current = head;
            while (current != null)
            {
                if (current.Data.Equals(data))
                    return true;
                current = current.Next;
            }
            return false;
        }
        /// <summary>
        /// Добавление элемента в начало списка
        /// </summary>
        /// <param name="data">Данные для добавления</param>
        public void AddFirst(T data)
        {
            Node<T> node = new Node<T>(data);
            Node<T> temp = head;
            node.Next = temp;
            head = node;
            if (Count == 0)
                tail = head;
            else
                temp.Previous = node;
            Count++;
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> current = head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        public IEnumerator GetEnumerator()
        {
            Node<T> current = head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        public IEnumerable<T> BackEnumerator()
        {
            Node<T> current = tail;
            while (current != null)
            {
                yield return current.Data;
                current = current.Previous;
            }
        }

        public object Clone()
        {
            var result = new DoublyList<T>();
            if (IsEmpty)
                return result;

            foreach (var elem in this)
                result.Add((T)elem);

            return result;
        }

        class Node<T>
        {
            public Node(T data)
            {
                Data = data;
            }

            public T Data { get; set; }
            public Node<T> Next { get; set; }
            public Node<T> Previous { get; set; }
        }
    }
}