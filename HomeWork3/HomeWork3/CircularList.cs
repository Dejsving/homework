﻿using System.Collections;
using System.Collections.Generic;

namespace HomeWork.Lists
{
    /// <summary>
    /// Кольцевой список
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CircularLinkedList<T> : IEnumerable<T>
    {
        Node<T> head; // головной/первый элемент
        Node<T> tail; // последний/хвостовой элемент
        public int Count { get; private set; }
        public bool IsEmpty { get { return Count == 0; } }

        /// <summary>
        /// Добавление элемента
        /// </summary>
        /// <param name="data">Данные для добавления</param>
        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);
            // если список пуст
            if (head == null)
            {
                head = node;
                tail = node;
                tail.Next = head;
            }
            else
            {
                node.Next = head;
                tail.Next = node;
                tail = node;
            }
            Count++;
        }

        /// <summary>
        /// Удаление элемента
        /// </summary>
        /// <param name="data">Данные для уделения</param>
        /// <returns>Успешно ли удаление</returns>
        public bool Remove(T data)
        {
            Node<T> current = head;
            Node<T> previous = null;

            if (IsEmpty) return false;

            do
            {
                if (current.Data.Equals(data))
                {
                    // Если узел в середине или в конце
                    if (previous != null)
                    {
                        // убираем узел current, теперь previous ссылается не на current, а на current.Next
                        previous.Next = current.Next;

                        // Если узел последний, изменяем переменную tail
                        if (current == tail)
                            tail = previous;
                    }
                    else // если удаляется первый элемент
                    {
                        // если в списке всего один элемент
                        if (Count == 1)
                        {
                            head = tail = null;
                        }
                        else
                        {
                            head = current.Next;
                            tail.Next = current.Next;
                        }
                    }
                    Count--;
                    return true;
                }

                previous = current;
                current = current.Next;
            }
            while (current != head);

            return false;
        }

        /// <summary>
        /// Очистка списка
        /// </summary>
        public void Clear()
        {
            head = null;
            tail = null;
            Count = 0;
        }

        /// <summary>
        /// Содержание элемента в списке
        /// </summary>
        /// <param name="data">Данные для поиска</param>
        /// <returns></returns>
        public bool Contains(T data)
        {
            Node<T> current = head;
            if (current == null) return false;
            do
            {
                if (current.Data.Equals(data))
                    return true;
                current = current.Next;
            }
            while (current != head);
            return false;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            Node<T> current = head;
            do
            {
                if (current != null)
                {
                    yield return current.Data;
                    current = current.Next;
                }
            }
            while (current != head);
        }

        public class Node<T>
        {
            public Node(T data)
            {
                Data = data;
            }
            public T Data { get; set; }
            public Node<T> Next { get; set; }
        }
    }
}