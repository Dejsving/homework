﻿using HomeWork.Lists;
using System;

namespace List
{
    class Program
    {
        public static readonly Random rnd = new Random();

        static void Main(string[] args)
        {
            CheckList();
            Console.ReadKey();
        }

        static void CheckList()
        {
            var list = new DoublyList<int>();
            for (int i = 0; i < 10; i++)
                list.Add(rnd.Next() % 10);

            foreach (var i in list)
                Console.WriteLine(i);

            var cloneList = (DoublyList<int>)list.Clone();

            Console.WriteLine("cloneList");
            foreach (var i in list)
                Console.WriteLine(i);
            cloneList.Remove(1);
            cloneList.Remove(2);
            cloneList.Remove(3);
            cloneList.Remove(4);
            cloneList.Remove(5);

            Console.WriteLine("list");
            foreach (var i in list)
                Console.WriteLine(i);

            Console.WriteLine("cloneList");
            foreach (var i in cloneList)
                Console.WriteLine(i);
        }

    }
}
