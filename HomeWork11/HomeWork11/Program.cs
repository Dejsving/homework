﻿using System;
using HomeWork11.Assistant;
using HomeWork11.Core;

namespace HomeWork11
{
    class Program
    {
        static void Main()
        {
            CheckClone();
            CheckMyClone();
            Console.ReadKey();
        }

        private static void CheckClone()
        {
            Console.WriteLine("Humans:");
            var human = RandomHumanGenerator.Generate();
            var humanClone = (Human) human.Clone();
            Console.WriteLine("Before change");
            Console.WriteLine($"Links: {human == humanClone}");
            Console.WriteLine($"Objects: {human.Equals(humanClone)}");
            humanClone.ReverseGender();
            Console.WriteLine("After change");
            Console.WriteLine($"Links: {human == humanClone}");
            Console.WriteLine($"Objects: {human.Equals(humanClone)}");

            Console.WriteLine("Citizens:");
            var passport = RandomPassportGenerator.Generate();
            var citizen = new Citizen(human, passport);
            var citizenClone = (Citizen) citizen.Clone();
            Console.WriteLine("Before change");
            Console.WriteLine($"Links: {citizen == citizenClone}");
            Console.WriteLine($"Objects: {citizen.Equals(citizenClone)}");
            citizenClone.Passport.Series = "2101";
            Console.WriteLine("After change");
            Console.WriteLine($"Links: {citizen == citizenClone}");
            Console.WriteLine($"Objects: {citizen.Equals(citizenClone)}");

            Console.WriteLine("Employees:");
            var company = RandomCompanyGenerator.Generate();
            var employee = new Employee(citizen, company);
            var employeeClone = (Employee) employee.Clone();
            Console.WriteLine("Before change");
            Console.WriteLine($"Links: {employee == employeeClone}");
            Console.WriteLine($"Objects: {employee.Equals(employeeClone)}");
            employeeClone.Company.Name = "Рога и Копыта";
            Console.WriteLine("After change");
            Console.WriteLine($"Links: {employee == employeeClone}");
            Console.WriteLine($"Objects: {employee.Equals(employeeClone)}");
        }

        private static void CheckMyClone()
        {
            Console.WriteLine("Humans:");
            var human = RandomHumanGenerator.Generate();
            var humanClone = human.MyClone();
            Console.WriteLine("Before change");
            Console.WriteLine($"Links: {human == humanClone}");
            Console.WriteLine($"Objects: {human.Equals(humanClone)}");
            humanClone.ReverseGender();
            Console.WriteLine("After change");
            Console.WriteLine($"Links: {human == humanClone}");
            Console.WriteLine($"Objects: {human.Equals(humanClone)}");

            Console.WriteLine("Citizens:");
            var passport = RandomPassportGenerator.Generate();
            var citizen = new Citizen(human, passport);
            var citizenClone = (Citizen)citizen.MyClone();
            Console.WriteLine("Before change");
            Console.WriteLine($"Links: {citizen == citizenClone}");
            Console.WriteLine($"Objects: {citizen.Equals(citizenClone)}");
            citizenClone.Passport.Series = "2101";
            Console.WriteLine("After change");
            Console.WriteLine($"Links: {citizen == citizenClone}");
            Console.WriteLine($"Objects: {citizen.Equals(citizenClone)}");

            Console.WriteLine("Employees:");
            var company = RandomCompanyGenerator.Generate();
            var employee = new Employee(citizen, company);
            var employeeClone = (Employee)employee.MyClone();
            Console.WriteLine("Before change");
            Console.WriteLine($"Links: {employee == employeeClone}");
            Console.WriteLine($"Objects: {employee.Equals(employeeClone)}");
            employeeClone.Company.Name = "Рога и Копыта";
            Console.WriteLine("After change");
            Console.WriteLine($"Links: {employee == employeeClone}");
            Console.WriteLine($"Objects: {employee.Equals(employeeClone)}");
        }
    }
}