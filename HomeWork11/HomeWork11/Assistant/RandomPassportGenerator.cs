﻿using Bogus;
using HomeWork11.Base;

namespace HomeWork11.Assistant
{
    public static class RandomPassportGenerator
    {
        public static Passport Generate()
        {
            var passportFaker = CreateFaker();
            return passportFaker.Generate();
        }

        private static Faker<Passport> CreateFaker()
        {
            var passportFaker = new Faker<Passport>()
                .CustomInstantiator(f => new Passport())
                .RuleFor(u => u.Series, (f, u) => f.Random.ReplaceNumbers("####"))
                .RuleFor(u => u.Number, (f, u) => f.Random.ReplaceNumbers("######"));

            return passportFaker;
        }
    }
}