﻿using Bogus;
using HomeWork11.Base;

namespace HomeWork11.Assistant
{
    public class RandomCompanyGenerator
    {
        public static Company Generate()
        {
            var companyFaker = CreateFaker();
            return companyFaker.Generate();
        }

        private static Faker<Company> CreateFaker()
        {
            var companyFaker = new Faker<Company>()
                .CustomInstantiator(f => new Company())
                .RuleFor(u => u.Address, (f, u) => f.Address.FullAddress())
                .RuleFor(u => u.Name, (f, u) => f.Company.CompanyName())
                .RuleFor(u => u.Commerce, (f, u) => f.Commerce.Ean13());

            return companyFaker;
        }
    }
}