﻿using System;
using Bogus;
using HomeWork11.Base;
using HomeWork11.Core;

namespace HomeWork11.Assistant
{
    public static class RandomHumanGenerator
    {
        public static Human Generate()
        {
            var humanFaker = CreateFaker();
            return humanFaker.Generate();
        }

        private static Faker<Human> CreateFaker()
        {
            var humanFaker = new Faker<Human>()
                .CustomInstantiator(f => new Human())
                .RuleFor(u => u.Name, (f, u) => f.Name.FirstName())
                .RuleFor(u => u.Surname, (f, u) => f.Name.LastName())
                .RuleFor(u => u.BirthDay, (f, u) => f.Date.Past(50, DateTime.Today))
                .RuleFor(u => u.Gender, (f, u) => f.Random.Enum<Gender>());

            return humanFaker;
        }
    }
}