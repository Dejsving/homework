﻿using System;

namespace HomeWork11.Base
{
    public class Passport : ICloneable, IMyCloneable<Passport>
    {
        public string Series { get; set; }
        public string Number { get; set; }

        public Passport() { }

        public Passport(string series, string number)
        {
            Series = series;
            Number = number;
        }

        public Passport(Passport source)
        {
            Series = source.Series;
            Number = source.Number;
        }

        public object Clone()
        {
            return new Passport(this);
        }

        public Passport MyClone()
        {
            return new Passport(this);
        }

        public override bool Equals(object obj)
        {
            Passport typeObj = obj as Passport;
            if (typeObj == null)
                return false;
            return Series.Equals(typeObj.Series) && Number.Equals(typeObj.Number);
        }
    }
}