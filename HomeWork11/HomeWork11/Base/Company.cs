﻿using System;

namespace HomeWork11.Base
{
    public class Company : ICloneable, IMyCloneable<Company>
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Commerce { get; set; }


        public Company() { }

        public Company(string name, string address, string commerce)
        {
            Name = name;
            Address = address;
            Commerce = commerce;
        }

        public Company(Company source)
        {
            Name = source.Name;
            Address = source.Address;
            Commerce = source.Commerce;
        }

        public object Clone()
        {
            return new Company(this);
        }

        public Company MyClone()
        {
            return new Company(this);
        }

        public override bool Equals(object obj)
        {
            Company typeObj = obj as Company;
            if (typeObj == null)
                return false;
            return Name.Equals(typeObj.Name) && Address.Equals(typeObj.Address) && Commerce.Equals(typeObj.Commerce);
        }
    }
}