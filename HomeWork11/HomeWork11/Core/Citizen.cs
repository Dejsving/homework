﻿using System;
using HomeWork11.Base;

namespace HomeWork11.Core
{
    public class Citizen : Human
    {
        public Passport Passport { get; set; }

        public Citizen(string name, string surname, Gender gender, DateTime birthDay, Passport passport)
            : base(name, surname, gender, birthDay)
        {
            Passport = passport;
        }

        public Citizen(Human human, Passport passport)
            : base(human)
        {
            Passport = passport;
        }

        public Citizen(Citizen source) : base(source)
        {
            if (source == null)
                throw new ArgumentNullException();
            Passport = (Passport)source.Passport.Clone();
        }

        public override Human MyClone()
        {
            return new Citizen(this);
        }

        public override object Clone()
        {
            return new Citizen(this);
        }

        public override bool Equals(object obj)
        {
            Citizen typeObj = obj as Citizen;
            if (typeObj == null)
                return false;
            return Passport.Equals(typeObj.Passport);
        }
    }
}