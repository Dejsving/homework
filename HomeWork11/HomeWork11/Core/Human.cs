﻿using System;
using HomeWork11.Base;

namespace HomeWork11.Core
{
    public class Human : ICloneable, IMyCloneable<Human>
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public Gender Gender { get; set; }
        public DateTime BirthDay { get; set; }

        public Human() { }

        public Human(string name, string surname, Gender gender, DateTime birthDay)
        {
            Name = name;
            Surname = surname;
            Gender = gender;
            BirthDay = birthDay;
        }

        public Human(Human source)
        {
            if (source == null)
                throw new ArgumentNullException();
            Name = source.Name;
            Surname = source.Surname;
            Gender = source.Gender;
            BirthDay = source.BirthDay;
        }

        public virtual object Clone()
        {
            return new Human(this);
        }

        public void ReverseGender()
        {
            if (Gender == Gender.Mail)
                Gender = Gender.Femail;
            else
                Gender = Gender.Mail;
        }

        public virtual Human MyClone()
        {
            return new Human(this);
        }

        public override string ToString()
        {
            return $"{Surname} {Name} : {Gender} : {BirthDay:d}";
        }
        public override bool Equals(object obj)
        {
            Human typeObj = obj as Human;
            if (typeObj == null)
                return false;
            return Name.Equals(typeObj.Name) && Surname.Equals(typeObj.Surname)
                   && Gender.Equals(typeObj.Gender) && BirthDay.Equals(typeObj.BirthDay);
        }
    }
}