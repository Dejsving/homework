﻿using System;
using HomeWork11.Base;

namespace HomeWork11.Core
{
    public class Employee : Citizen
    {
        public Company Company { get; set; }

        public Employee(string name, string surname, Gender gender, DateTime birthDay, Passport passport, Company company)
            : base(name, surname, gender, birthDay, passport)
        {
            Company = company;
        }

        public Employee(Citizen citizen, Company company)
            : base(citizen)
        {
            Company = company;
        }

        public Employee(Employee source) : base(source)
        {
            if (source == null)
                throw new ArgumentNullException();
            Company = (Company) source.Company.Clone();
        }

        public override Human MyClone()
        {
            return new Employee(this);
        }

        public override object Clone()
        {
            return new Employee(this);
        }

        public override bool Equals(object obj)
        {
            Employee typeObj = obj as Employee;
            if (typeObj == null)
                return false;
            return Company.Equals(typeObj.Company);
        }
    }
}