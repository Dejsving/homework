﻿namespace HomeWork11
{
    public interface IMyCloneable<T>
    {
        T MyClone();
    }
}