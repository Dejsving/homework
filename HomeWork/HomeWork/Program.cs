﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;

public interface IHasher
{
    string Hash(string value);
}

public class Hasher : IHasher
{
    public string Hash(string value)
    {
        using (SHA256 hash = SHA256Managed.Create())
        {
            return String.Concat(hash
                .ComputeHash(Encoding.UTF8.GetBytes(value))
                .Select(item => item.ToString("x2")));
        }
    }
}

public class User
{
    public string Login { get; set; }
    public string Name { get; set; }
    public string Password { get; set; }
    public Role Role { get; set; }
}

public enum Role
{
    Guest,
    User,
    Administrator
}

public class AuthManager
{
    private IHasher hasher;

    private List<User> users = new List<User>();

    public AuthManager(IHasher _hasher)
    {
        hasher = _hasher;
    }

    public void RegisterUser(User user)
    {
        users.Add(new User() { Login = user.Login, Name = user.Name, Password = hasher.Hash(user.Login + user.Password) });
    }

    public bool VerifyUser(User user)
    {
        return users.Where(
                       x => string.Compare(x.Login, user.Login) == 0 && string.Compare(x.Password, hasher.Hash(user.Login + user.Password)) == 0)
                   .Count() != 0;
    }

    public void AddRole(User user, Role role)
    {
        user.Role = role;
    }

    public bool CheckRole(User user, Role role)
    {
        if (user.Role > role)
            return true;
        return false;
    }
}


public class Program
{

    public static void Main()
    {
        AuthManager manager = new AuthManager(new Hasher());
        var user1 = new User() {Login = "gg@gg.gg", Name = "User1", Password = "123"};
        var user2 = new User() {Login = "gg1@gg.gg", Name = "User2", Password = "1234"};
        var user3 = new User() {Login = "gg2@gg.gg", Name = "User3", Password = "1235"};

        manager.RegisterUser(user1);
        manager.RegisterUser(user2);
        manager.RegisterUser(user3);

        manager.AddRole(user1, Role.Guest);
        manager.AddRole(user2, Role.User);
        manager.AddRole(user3, Role.Administrator);

        Console.WriteLine("user1");
        Console.WriteLine(manager.CheckRole(user1, Role.Guest));
        Console.WriteLine(manager.CheckRole(user1, Role.User));
        Console.WriteLine(manager.CheckRole(user1, Role.Administrator));

        Console.WriteLine("user2");
        Console.WriteLine(manager.CheckRole(user2, Role.Guest));
        Console.WriteLine(manager.CheckRole(user2, Role.User));
        Console.WriteLine(manager.CheckRole(user2, Role.Administrator));

        Console.WriteLine("user3");
        Console.WriteLine(manager.CheckRole(user3, Role.Guest));
        Console.WriteLine(manager.CheckRole(user3, Role.User));
        Console.WriteLine(manager.CheckRole(user3, Role.Administrator));

        Console.ReadKey();
    }
}