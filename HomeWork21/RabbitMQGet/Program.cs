﻿using System;
using System.Runtime.InteropServices;
using HomeWork21;
using System.Threading;

namespace RabbitMQGet
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start");
            Thread.Sleep(100);
            Console.WriteLine("Start getting");
            using (var rabbit = new RabbitMq())
            {
                rabbit.GetMessage();
                var key = Console.ReadKey();
                while (key.Key != ConsoleKey.Escape)
                {
                    key = Console.ReadKey();
                }
            }
        }
    }
}
