﻿using System;
using System.Threading;
using HomeWork21;

namespace RabbitMQSend
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start");
            using (var rabbit = new RabbitMq())
            {
                while (true)
                {
                    var key = Console.ReadKey();
                    if (key.Key == ConsoleKey.Enter)
                    {
                        Console.WriteLine($"Отправлено сообщение: {rabbit.SendMessage(RandomUserGenerator.Generate())}");
                        Thread.Sleep(1000);
                    }
                    if (key.Key == ConsoleKey.Escape)
                        return;
                }
            }
        }
    }
}
