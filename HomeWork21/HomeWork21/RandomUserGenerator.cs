﻿using System;
using Bogus;

namespace HomeWork21
{
    public class RandomUserGenerator
    {
        static Random rnd = new Random();

        public static User Generate()
        {
            var customersFaker = CreateFaker();
            var user = customersFaker.Generate();
            if (ChangeEmail())
                user.Email = "";
            return user;
        }

        private static Faker<User> CreateFaker()
        {
            var customersFaker = new Faker<User>()
                .CustomInstantiator(f => new User())
                .RuleFor(u => u.Name, (f, u) => f.Name.FirstName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.Name))
                .RuleFor(u => u.Age, (f, u) => f.Random.Int(0, 100));

            return customersFaker;
        }

        private static bool ChangeEmail()
        {
            return rnd.Next() % 3 == 0;
        }
    }
}