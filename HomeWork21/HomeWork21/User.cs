﻿namespace HomeWork21
{
    public class User
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }

        public override string ToString()
        {
            return $"{Name} - {Age} - {(string.IsNullOrWhiteSpace(Email) ? "null" : Email)}";
        }
    }
}