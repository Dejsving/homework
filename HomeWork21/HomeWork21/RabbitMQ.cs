﻿using System;
using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace HomeWork21
{
    public class RabbitMq : IDisposable
    {
        private ConnectionFactory Factory { get; }
        private IConnection Connection { get; }
        private IModel Channel { get; }
        public RabbitMq()
        {
            Factory = new ConnectionFactory()
            {
                HostName = "kangaroo.rmq.cloudamqp.com",
                VirtualHost = "cougdvlh",
                UserName = "cougdvlh",
                Password = "HiDSAQ0pG60ZXwKUVYbnT-KJ4v0x3vH2"
            };
            Connection = Factory.CreateConnection();
            Channel = Connection.CreateModel();
        }

        public string SendMessage(User user)
        {
            Channel.QueueDeclare("homework", false, false, false, null);
            string message = JsonConvert.SerializeObject(user);
            var body = Encoding.UTF8.GetBytes(message);
            Channel.BasicPublish("", "homework", null, body);
            return user.ToString();
        }

        public void GetMessage()
        {
            Channel.QueueDeclare("homework", false, false, false, null);
            var consumer = new EventingBasicConsumer(Channel);
            consumer.Received += HandlingMessage;
            Channel.BasicConsume(consumer, "homework");
        }

        private void HandlingMessage(object model, BasicDeliverEventArgs ea)
        {
            var body = ea.Body;
            var message = JsonConvert.DeserializeObject<User>(Encoding.UTF8.GetString(body));
            if (string.IsNullOrWhiteSpace(message.Email))
            {
                Channel.BasicNack(ea.DeliveryTag, false, false);
                return;
            }
            Channel.BasicAck(ea.DeliveryTag, false);
            Console.WriteLine(message);
        }

        public void Dispose()
        {
            if (Channel != null && !Channel.IsClosed)
                Channel.Close();
            if (Connection != null && Connection.IsOpen)
                Connection.Close();
        }
    }
}