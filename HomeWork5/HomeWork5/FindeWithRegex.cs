﻿using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace Regex
{
    public static class FindeWithRegex
    {
        public static string CheckRegex()
        {
            StringBuilder sb = new StringBuilder();
            // В качестве примера - страница Яндекса с запросом на поиск "url"
            var url = @"https://yandex.ru/search/?text=url&clid=2186621&lr=213&redircnt=1575196569.1";

            string downloadedUrl;
            using (WebClient client = new WebClient())
                downloadedUrl = client.DownloadString(url);

            System.Text.RegularExpressions.Regex regex =
                new System.Text.RegularExpressions.Regex(@"href\s*=\s*(?:([""']([^'""]+)[""'])|([^\s>]+))");
            MatchCollection matches = regex.Matches(downloadedUrl);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                    sb.AppendLine(match.Value);
            }
            else
            {
                return "Совпадений не найдено";
            }

            return sb.ToString();
        }
    }
}