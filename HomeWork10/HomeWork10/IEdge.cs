﻿namespace HomeWork10
{
    public interface IEdge<T>
    {
        INode<T> FromNode { get; set; }
        INode<T> ToNode { get; set; }
        double Weight { get; set; }

        Edge<T> Reverse();
    }
}