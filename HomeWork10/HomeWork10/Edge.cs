﻿using System;

namespace HomeWork10
{
    public class Edge<T> : IEdge<T>
    {
        public INode<T> FromNode { get; set; }
        public INode<T> ToNode { get; set; }
        public double Weight { get; set; }

        public override string ToString()
        {
            return $"{FromNode} <=> {ToNode} <=> {Weight}";
        }

        public Edge<T> Reverse()
        {
            return new Edge<T>(ToNode, FromNode, Weight);
        }

        public Edge(INode<T> from, INode<T> to, double weight)
        {
            FromNode = from;
            ToNode = to;
            Weight = weight;
        }

        public override int GetHashCode()
        {
            return FromNode.Id + ToNode.Id + (int) Weight;
        }

        public override bool Equals(object e)
        {
            if (e == null)
                return false;
            Edge<T> edge = e as Edge<T>;
            if (edge == null)
                return false;
            return ToNode.Equals(edge.ToNode) && FromNode.Equals(edge.FromNode) && Math.Abs(Weight - edge.Weight) < double.Epsilon;
        }

        public static bool operator ==(Edge<T> e1, Edge<T> e2)
        {
            if ((object)e1 == null)
                return false;
            return e1.Equals(e2);
        }

        public static bool operator !=(Edge<T> e1, Edge<T> e2)
        {
            return !(e1 == e2);
        }
    }
}