﻿using System.Xml;

namespace HomeWork10
{
    public class Node<T> : INode<T>
    {
        public int Id { get; set; }
        public T Data { get; set; }

        public Node()
        {
            Id = 0;
            Data = default(T);
        }

        public Node(int id)
        {
            Id = id;
            Data = default(T);
        }

        public Node(int id, T data)
        {
            Id = id;
            Data = data;
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public override string ToString()
        {
            return $"{Id}->{(Data == null ? "null" : Data.ToString())}";
        }

        public override bool Equals(object z)
        {
            if (z == null)
                return false;
            Node<T> node = z as Node<T>;
            if (node == null)
                return false;
            return Id == node.Id;
        }

        public static bool operator ==(Node<T> n1, Node<T> n2)
        {
            if ((object)n1 == null)
                return false;
            return n1.Equals(n2);
        }

        public static bool operator !=(Node<T> n1, Node<T> n2)
        {
            return !(n1 == n2);
        }
    }
}