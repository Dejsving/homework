﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace HomeWork10
{
    public class Graph<T>
    {
        int Order { get; set; }
        int Size { get; set; }
        public List<INode<T>> Nodes { get; set; }
        public List<IEdge<T>> Edges { get; set; }

        #region Add
        public void AddNode(int id)
        {
            AddNode(new Node<T>(id));
        }
        public void AddNode(int id, T value)
        {
            AddNode(new Node<T>(id, value));
        }
        public void AddNode(INode<T> node)
        {
            if (Contains(node))
                throw new ArgumentException($"Элемент с таким Id={node.Id} уже есть");
            Nodes.Add(node);
        }
        #endregion
        #region Contains
        public bool Contains(INode<T> node)
        {
            return Nodes.Contains(node);
        }
        public bool Contains(int id)
        {
            return Nodes.Select(t => t.Id).Contains(id);
        }
        public bool Contains(T value)
        {
            return Nodes.Select(t => t.Data).Contains(value);
        }
        #endregion
        #region Remove
        public void Remove(INode<T> elem)
        {
            if (!Contains(elem))
                throw new ArgumentException($"Элемент {elem.Id} не найден");
            Edges.RemoveAll(t => t.FromNode.Equals(elem) || t.ToNode.Equals(elem));
            Nodes.Remove(elem);
        }
        public void Remove(int id)
        {
            Remove(Nodes.Find(t => t.Id == id));
        }
        #endregion
        #region GetValue
        public T GetValue(int id)
        {
            return Nodes.Find(t => t.Id == id).Data;
        }
        public T GetValue(INode<T> elem)
        {
            return GetValue(elem.Id);
        }
        #endregion
        #region AddEdge
        public void AddEdge(INode<T> from, INode<T> to, double weight = 1)
        {
            if (!Contains(from))
                throw new ArgumentException($"Элемент с таким Id={from.Id} отсутствует в графе");
            if (!Contains(to))
                throw new ArgumentException($"Элемент с таким Id={to.Id} отсутствует в графе");
            Edges.Add(new Edge<T>(from, to, weight));
        }
        #endregion
        #region RemoveEdge
        public void RemoveEdge(IEdge<T> edge)
        {
            Edges.Remove(edge);
        }
        public void RemoveEdge(INode<T> from, INode<T> to)
        {
            var elemToRemove = Edges.Find(t => t.FromNode.Equals(from) && t.ToNode.Equals(to));
            RemoveEdge(elemToRemove);
        }
        #endregion
        public void FillNodesData(T[] dataForNodes)
        {
            for (int i = 0; i < dataForNodes.Length; i++)
            {
                Nodes[i].Data = dataForNodes[i];
            }
        }
        #region Constructors
        public Graph()
        {
            Nodes = new List<INode<T>>();
            Edges = new List<IEdge<T>>();
            Order = 0;
            Size = 0;
        }

        Graph(List<INode<T>> nodes, List<IEdge<T>> edges)
        {
            Nodes = nodes;
            Edges = edges;
            Order = Nodes.Count;
            Size = Edges.Count;
        }

        public Graph(string filePath, FileType fileType)
        {
            Graph<T> tempGraph;
            if (fileType == FileType.AdjacencyMatrix)
                tempGraph = FromAdjacencyMatrix(filePath);
            else if (fileType == FileType.IncidenceMatrix)
                tempGraph = FromIncidenceMatrix(filePath);
            else if (fileType == FileType.ListIncidents)
                tempGraph = FromListIncidents(filePath);
            else if (fileType == FileType.ListEdges)
                tempGraph = FromListEdges(filePath);
            else
                throw new ArgumentException("Неверный тип файла", nameof(fileType));

            Nodes = tempGraph.Nodes;
            Edges = tempGraph.Edges;
            Order = tempGraph.Nodes.Count;
            Size = tempGraph.Edges.Count;
        }

        private Graph<T> FromAdjacencyMatrix(string filePath)
        {
            double[,] matrix = null;
            using (StreamReader file = new StreamReader(filePath))
            {
                var i = 0;
                while (!file.EndOfStream)
                {
                    var temp = file.ReadLine();
                    if (string.IsNullOrWhiteSpace(temp))
                        continue;
                    var splitString = temp.Split(';');
                    var len = splitString.Length;
                    if (matrix == null)
                        matrix = new double[len, len];

                    for (int j = 0; j < len; j++)
                    {
                        if (!double.TryParse(splitString[j], out matrix[i, j]))
                            throw new ArgumentException($"Элемент матрицы [{i},{j}] не удается преобразовать в double");
                    }

                    i++;
                }
            }

            if (matrix.GetLength(0) != matrix.GetLength(1))
                throw new ArgumentException("Матрица смежности должна быть квадратная");

            var order = matrix.GetLength(0);

            var nodes = new List<INode<T>>(order);
            for (var i = 0; i < order; i++)
            {
                nodes.Add(new Node<T>(i + 1));
            }

            var edges = new List<IEdge<T>>();
            for (var i = 0; i < order; i++)
            {
                for (int j = 0; j < order; j++)
                {
                    if (matrix[i, j] > double.Epsilon)
                        edges.Add(new Edge<T>(nodes[i], nodes[j], matrix[i, j]));
                }
            }

            return new Graph<T>(nodes, edges);
        }

        private Graph<T> FromIncidenceMatrix(string filePath)
        {
            double[,] matrix;
            var listSplitStrings = new List<string[]>();
            using (StreamReader file = new StreamReader(filePath))
            {
                while (!file.EndOfStream)
                {
                    var temp = file.ReadLine();
                    if (string.IsNullOrWhiteSpace(temp))
                        continue;
                    listSplitStrings.Add(temp.Split(';'));
                }
            }

            matrix = new double[listSplitStrings.Count, listSplitStrings[0].Length];

            for (var i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (!double.TryParse(listSplitStrings[i][j], out matrix[i, j]))
                        throw new ArgumentException($"Элемент матрицы [{i},{j}] не удается преобразовать в double");
                }
            }

            var order = matrix.GetLength(0);
            var size = matrix.GetLength(1);

            var nodes = new List<INode<T>>(order);
            for (var i = 0; i < order; i++)
            {
                nodes.Add(new Node<T>(i + 1));
            }

            var edges = new List<IEdge<T>>(size);
            for (var j = 0; j < size; j++)
            {
                int fromIndex = -1, toPositiveIndex = -1, toNegativeIndex = -1;

                for (int i = 0; i < order; i++)
                {
                    if (matrix[i, j] > 0)
                    {
                        if (fromIndex == -1)
                            fromIndex = i;
                        else
                            toPositiveIndex = i;
                    }

                    if (matrix[i, j] < 0)
                        toNegativeIndex = i;
                }

                if (toNegativeIndex == -1) // значит у нас двунаправленное ребро
                {
                    edges.Add(new Edge<T>(nodes[fromIndex], nodes[toPositiveIndex], matrix[fromIndex, j]));
                    edges.Add(new Edge<T>(nodes[toPositiveIndex], nodes[fromIndex], matrix[toPositiveIndex, j]));
                }
                else
                {
                    edges.Add(new Edge<T>(nodes[fromIndex], nodes[toNegativeIndex], matrix[fromIndex, j]));
                }
            }

            return new Graph<T>(nodes, edges);
        }

        private Graph<T> FromListIncidents(string filePath)
        {
            var incidenceStrings = GetStringsFromFile(filePath);
            var (fromNodes, toNodes) = ConvertNodesFromString(incidenceStrings);
            var nodes = GenerateNodes(fromNodes, toNodes);
            var edges = GenerateEdges(fromNodes, toNodes, nodes);
            return new Graph<T>(nodes, edges);
        }

        #region FromListIncidentsExtensions
        private List<string> GetStringsFromFile(string filePath)
        {
            List<string> fileInto = new List<string>();
            using (StreamReader file = new StreamReader(filePath))
            {
                while (!file.EndOfStream)
                {
                    var temp = file.ReadLine();
                    if (!string.IsNullOrWhiteSpace(temp))
                        fileInto.Add(temp);
                }
            }

            return fileInto;
        }

        private (List<int> fromNodes, List<List<int>> toNodes) ConvertNodesFromString(List<string> fileInto)
        {
            List<int> fromNodes = new List<int>();
            List<List<int>> toNodes = new List<List<int>>();

            foreach (var str in fileInto)
            {
                toNodes.Add(new List<int>());
                var toIdFromIds = str.Split(';');
                fromNodes.Add(int.Parse(toIdFromIds[0]));
                var fromIds = toIdFromIds[1].Split(',');
                foreach (var fromId in fromIds)
                {
                    toNodes.Last().Add(int.Parse(fromId));
                }
            }

            return (fromNodes, toNodes);
        }

        private List<INode<T>> GenerateNodes(List<int> fromNodes, List<List<int>> toNodes)
        {
            var nodes = new List<INode<T>>();
            var nodesIds = new List<int>(fromNodes);
            nodesIds.AddRange(toNodes.SelectMany(t => t));
            nodesIds = nodesIds.Distinct().ToList();

            foreach (var nodesId in nodesIds)
            {
                nodes.Add(new Node<T>(nodesId));
            }

            return nodes;
        }

        private List<IEdge<T>> GenerateEdges(List<int> fromNodes, List<List<int>> toNodes, List<INode<T>> nodes)
        {
            var edges = new List<IEdge<T>>();
            for (int i = 0; i < fromNodes.Count; i++)
            {
                for (int j = 0; j < toNodes[i].Count; j++)
                {
                    edges.Add(new Edge<T>(nodes.Find(t => t.Id == fromNodes[i]),
                        nodes.Find(t => t.Id == toNodes[i][j]), 1));
                }
            }

            return edges;
        }
        #endregion

        private Graph<T> FromListEdges(string filePath)
        {
            var edgeStrings = GetStringsFromFile(filePath);

            var nodes = new List<INode<T>>();
            var edge = new List<IEdge<T>>();

            foreach (var edgeString in edgeStrings)
            {
                var splitString = edgeString.Split(';');
                var fromId = int.Parse(splitString[1]);
                var toId = int.Parse(splitString[2]);
                var weight = double.Parse(splitString[3]);

                if (!nodes.Select(t => t.Id).Contains(fromId))
                    nodes.Add(new Node<T>(fromId));
                if (!nodes.Select(t => t.Id).Contains(toId))
                    nodes.Add(new Node<T>(toId));

                edge.Add(new Edge<T>(
                    nodes.Find(t => t.Id == fromId),
                    nodes.Find(t => t.Id == toId),
                    weight));
            }

            return new Graph<T>(nodes, edge);
        }
        #endregion
        #region SaveToFile
        public void SaveToFile(string fileName, FileType fileType)
        {
            if (fileType == FileType.AdjacencyMatrix)
                SaveMatrixToFile(GetAdjacencyMatrix(), fileName);
            else if (fileType == FileType.IncidenceMatrix)
                SaveMatrixToFile(GetIncidenceMatrix(), fileName);
            else if (fileType == FileType.ListIncidents)
                SaveToListIncidents(fileName);
            else if (fileType == FileType.ListEdges)
                SaveToListEdges(fileName);
            else
                throw new ArgumentException("Неверный тип файла", nameof(fileType));
        }

        private void SaveToListIncidents(string fileName)
        {
            var sb = new StringBuilder();
            foreach (var node in Nodes)
            {
                sb.Append($"{node.Id};");
                sb.AppendLine(string.Join(",", Edges.Where(t => t.FromNode.Id == node.Id).Select(t => t.ToNode.Id)));
            }

            using (StreamWriter file = new StreamWriter(fileName))
            {
                file.WriteLine(sb.ToString());
            }
        }

        private void SaveToListEdges(string fileName)
        {
            var sb = new StringBuilder();
            var i = 0;
            foreach (var edge in Edges)
            {
                i++;
                sb.AppendLine($"{i};{edge.FromNode.Id};{edge.ToNode.Id};{edge.Weight}");
            }

            using (StreamWriter file = new StreamWriter(fileName))
            {
                file.WriteLine(sb.ToString());
            }
        }

        private void SaveMatrixToFile(double[,] matrix, string fileName)
        {
            using (StreamWriter file = new StreamWriter(fileName))
            {
                file.WriteLine(
                    string.Join(Environment.NewLine,
                        Enumerable.Range(0, matrix.GetLength(0)).
                            Select(t => string.Join(";",
                                Enumerable.Range(0, matrix.GetLength(1)).Select(s => matrix[t, s])))));
            }
        }
        #endregion

        public double[,] GetAdjacencyMatrix()
        {
            var matrix = new double[Order, Order];
            Array.Clear(matrix, 0, matrix.Length);

            foreach (var edge in Edges)
            {
                matrix[edge.FromNode.Id - 1, edge.ToNode.Id - 1] = edge.Weight;
            }

            return matrix;
        }

        private double[,] GetIncidenceMatrix()
        {
            var matrix = new double[Order, Size];
            Array.Clear(matrix, 0, matrix.Length);

            foreach (var edge in Edges)
            {
                var indexEdge = Edges.FindIndex(t => t == edge);
                matrix[edge.FromNode.Id - 1, indexEdge] = edge.Weight;
                matrix[edge.ToNode.Id - 1, indexEdge] = -1 * edge.Weight;
            }

            return matrix;
        }

        public override int GetHashCode()
        {
            return Order + Size;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            Graph<T> graf = obj as Graph<T>;
            if (graf == null)
                return false;
            if (Order != graf.Order || Size != graf.Size)
                return false;
            var thisMatrix = GetAdjacencyMatrix();
            var objMatrix = graf.GetAdjacencyMatrix();
            var result = true;
            for (int i = 0; i < Order; i++)
            {
                for (int j = 0; j < Order; j++)
                {
                    result = result && Math.Abs(thisMatrix[i, j] - objMatrix[i, j]) < double.Epsilon;
                }
            }
            return result;
        }
    }
}