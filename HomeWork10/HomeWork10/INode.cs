﻿using System.Runtime.InteropServices;

namespace HomeWork10
{
    public interface INode<T>
    {
        int Id { get; set; }
        T Data { get; set; }
    }
}