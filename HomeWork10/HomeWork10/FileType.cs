﻿namespace HomeWork10
{
    public enum FileType
    {
        AdjacencyMatrix,
        IncidenceMatrix,
        ListIncidents,
        ListEdges
    }
}