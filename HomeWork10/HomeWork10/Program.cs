﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork10
{
    class Program
    {
        static void Main(string[] args)
        {
            var GraphA = new Graph<string>("AdjacencyMatrix.csv", FileType.AdjacencyMatrix);
            GraphA.SaveToFile("IncidenceMatrix.csv", FileType.IncidenceMatrix);
            var GraphI = new Graph<string>("IncidenceMatrix.csv", FileType.IncidenceMatrix);
            GraphA.SaveToFile("ListIncidents.csv", FileType.ListIncidents);
            var GraphLI = new Graph<string>("ListIncidents.csv", FileType.ListIncidents);
            GraphA.SaveToFile("ListEdges.csv", FileType.ListEdges);
            var GraphLE = new Graph<string>("ListEdges.csv", FileType.ListEdges);

            Console.WriteLine(GraphA.Equals(GraphI));
            Console.WriteLine(GraphA.Equals(GraphLI));
            Console.WriteLine(GraphA.Equals(GraphLE));
            Console.ReadKey();
        }
    }
}
