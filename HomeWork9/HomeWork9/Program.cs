﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork9
{
    class Program
    {
        static private string directory = @"c:\source\repos\Temp\";
        static void Main(string[] args)
        {
            MaxElement();
            FilesInDirectory(directory);
            Console.ReadKey();
        }

        private static void MaxElement()
        {
            var list = new List<Complex>
            {
                new Complex(1, 1),
                new Complex(1, 2),
                new Complex(1, 3),
                new Complex(1, 4),
                new Complex(1, 5),
                new Complex(2, 1),
                new Complex(2, 2),
                new Complex(2, 3),
                new Complex(2, 4),
                new Complex(2, 5),
                new Complex(3, 1),
                new Complex(3, 2),
                new Complex(3, 3),
                new Complex(3, 4),
                new Complex(3, 5)
            };
            var vsMax = list.Max(complex => complex.GetHashCode());
            var myMax = list.GetMax(complex => complex.GetHashCode());
            Console.WriteLine($"{list.Find(c => Math.Abs(c.GetHashCode() - vsMax) < double.Epsilon * 1000)} <=> {myMax}");
        }

        private static void FilesInDirectory(string directory)
        {
            var dir = new LookForDirectory(directory);
            dir.fileArgs += FileIsFound;
            dir.ForAllFiles();
        }

        static void FileIsFound(object sender, FileArgs e)
        {
            Console.WriteLine($"{e.Number}:\t{e.FindTime}\tНайден файл {e.Name}");
        }
    }
}
