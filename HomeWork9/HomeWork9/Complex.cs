﻿using System;

namespace HomeWork9
{
    /// <summary>
    /// Клас комплексных чисел
    /// </summary>
    public class Complex
    {
        private double a, b;
        public const double pi = 3.1415;
        #region Конструкторы
        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public Complex()
        {
            a = 0;
            b = 0;
        }
        /// <summary>
        /// Классическое задание комплексного числа
        /// </summary>
        /// <param name="x">Действительная часть числа</param>
        /// <param name="y">Мнимая часть числа</param>
        public Complex(double x, double y)
        {
            a = x;
            b = y;
        }
        /// <summary>
        /// Расширение double числа до комплексного
        /// </summary>
        /// <param name="Re">Действительная часть числа</param>
        public Complex(double Re)
        {
            a = Re;
            b = 0;
        }
        /// <summary>
        /// Создание комплексного числа по методу Эйлера
        /// </summary>
        /// <param name="r">Радиус</param>
        /// <param name="phi">Угол</param>
        /// <returns></returns>
        public static Complex Euler(double r, double phi)
        {
            return new Complex(r * Math.Cos(phi), r * Math.Sin(phi));
        }
        #endregion
        #region Свойства
        public double re
        {
            get { return a; }
            set { a = value; }
        }
        public double im
        {
            get { return b; }
            set { b = value; }
        }
        public double mod
        {
            get { return Math.Sqrt(re * re + im * im); }
        }
        public double arg
        {
            get
            {
                if (mod == 0) return 0;
                else if (im > 0) return Math.Acos(re / mod);
                else return -Math.Acos(re / mod);
            }
        }
        #endregion
        public override string ToString()
        {
            if (a == 0 && b == 0)
                return "0";
            if (a == 0 && b == 1)
                return "i";
            if (a == 0 && b == -1)
                return "-i";
            if (a == 0)
                return b.ToString() + 'i';
            if (b == 0)
                return a.ToString();
            if (b == 1)
                return a.ToString() + '+' + 'i';
            if (b == -1)
                return a.ToString() + '-' + 'i';
            if (b > 0)
                return a.ToString() + '+' + b + 'i';
            return a + b.ToString() + 'i';
        }
        #region static
        public static Complex operator +(Complex z1, Complex z2)
        {
            return new Complex(z1.re + z2.re, z1.im + z2.im);
        }
        public static Complex operator -(Complex z1, Complex z2)
        {
            return new Complex(z1.re - z2.re, z1.im - z2.im);
        }
        public static Complex operator *(Complex z1, Complex z2)
        {
            return new Complex(z1.re * z2.re + z1.im * z2.im, z1.re * z2.im + z1.im * z2.re);
        }
        public static Complex Conjugate(Complex z)
        {
            return new Complex(z.re, -z.im);
        }
        public static Complex operator /(Complex z, double u)
        {
            if (u != 0) return new Complex(z.re / u, z.im / u);
            else return null;
        }
        public static Complex operator /(Complex z1, Complex z2)
        {
            if (z2 == 0) return null;
            var z = z1 * Conjugate(z2);
            z /= z2.mod * z2.mod;
            return z;
        }
        public static Complex pow(Complex z, int u)
        {
            return Euler(Math.Pow(z.mod, u), z.arg * u);
        }
        public static Complex[] radical(Complex z, int u)
        {
            Complex[] radic = new Complex[u];
            double module = Math.Pow(z.mod, (double)1 / u);
            double promArg = z.arg / u;
            for (int i = 0; i < u; i++)
            {
                radic[i] = Euler(module, promArg + 2 * pi * i / u);
            }
            return radic;
        }
        #endregion
        #region Типы
        public static explicit operator double(Complex z)
        {
            return z.re;
        }
        public static implicit operator Complex(double x)
        {
            return new Complex(x);
        }
        #endregion
        public override int GetHashCode()
        {
            return (int)(re + im);
        }
        #region Индексатор
        public double this[int i]
        {
            get
            {
                if (i == 0)
                    return re;
                if (i == 1)
                    return im;
                throw new ArgumentOutOfRangeException();
            }
            set
            {
                if (i == 0)
                    re = value;
                else if (i == 1)
                    im = value;
                else
                    throw new ArgumentOutOfRangeException();
            }
        }
        #endregion
        #region Bool
        public override bool Equals(object z)
        {
            if (z == null)
                return false;
            Complex zObj = z as Complex;
            if (zObj == null)
                return false;
            return re == zObj.re && im == zObj.im;
        }
        public static bool operator ==(Complex z1, Complex z2)
        {
            if ((object)z1 == null)
                return false;
            return z1.Equals(z2);
        }
        public static bool operator !=(Complex z1, Complex z2)
        {
            return !(z1 == z2);
        }
        #endregion
    }
}