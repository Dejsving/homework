﻿using System;
using System.IO;
using System.Text;
using System.Threading;

namespace HomeWork9
{
    public class LookForDirectory
    {
        public int Count;
        public DirectoryInfo Directory { get; }
        public event EventHandler<FileArgs> fileArgs;

        public LookForDirectory(string dir)
        {
            Count = 0;
            Directory = new DirectoryInfo(dir);
        }

        public void ForAllFiles()
        {
            foreach (var file in Directory.GetFiles())
            {
                FileArgs args = new FileArgs();
                args.FindTime = DateTime.Now;
                args.Number = Count++;
                args.Name = file.Name;
                OnFileFound(args);
            }
        }
        protected virtual void OnFileFound(FileArgs e)
        {
            EventHandler<FileArgs> handler = fileArgs;
            handler?.Invoke(this, e);
        }
    }
}