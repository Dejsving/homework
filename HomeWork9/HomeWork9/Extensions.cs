﻿using System;
using System.Collections.Generic;

namespace HomeWork9
{
    public static class Extensions
    {
        private static readonly double Eps = 1e-5;
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, double> getParametr) where T : class
        {
            var enumerator = e.GetEnumerator();
            if (!enumerator.MoveNext())
                throw new InvalidOperationException("Последовательность не содержит элементов");
            var max = enumerator.Current;
            foreach (var elem in e)
            {
                if (getParametr(elem) - getParametr(max) > Eps)
                    max = elem;
            }
            return max;
        }
    }
}