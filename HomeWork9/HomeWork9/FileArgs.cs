﻿using System;

namespace HomeWork9
{
    public class FileArgs : EventArgs
    {
        public int Number { get; set; }
        public DateTime FindTime { get; set; }
        public string Name { get; set; }
    }
}