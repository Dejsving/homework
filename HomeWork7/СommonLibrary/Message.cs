﻿using Newtonsoft.Json;
using System;

namespace СommonLibrary
{
    [Serializable]
    public class Message
    {
        private int _id;
        /// <summary>
        /// Id сообщения
        /// </summary>
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private DateTime _sendDate;
        /// <summary>
        /// Дата и время отправления
        /// </summary>
        public DateTime SendDate
        {
            get { return _sendDate; }
            set { _sendDate = value; }
        }

        private string _name;
        /// <summary>
        /// Имя отправителя
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _textData;
        /// <summary>
        /// Сообщение
        /// </summary>
        public string TextData
        {
            get { return _textData; }
            set { _textData = value; }
        }

        public override string ToString()
        {
            return Id.ToString() + "\t" + Name + "\t" + SendDate.ToString() + "\t" + TextData;
        }

        public Message() {}

        public Message(string name, string textData, Random rnd)
        {
            Name = name;
            TextData = textData;
            SendDate = DateTime.Now;
            Id = rnd.Next();
        }

        /// <summary>
        /// Получить строку сериализации для отправки сообщения
        /// </summary>
        /// <returns>Сериализованные данные</returns>
        public string GetSendString()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// Метод для десериализации данных из строки
        /// </summary>
        /// <param name="input">Строка сериализованных данных</param>
        /// <returns></returns>
        public static Message GetSendMessage(string input)
        {
            return JsonConvert.DeserializeObject<Message>(input);
        }
    }
}
