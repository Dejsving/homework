﻿using System;
using System.Configuration;
using System.Threading;

namespace Server
{
    class Program
    {
        static int port = 8888;
        static string address = "127.0.0.1";
        static ServerObject server;
        static Thread listenThread;
        static void Main(string[] args)
        {
            ReadAllSettings();

            try
            {
                server = new ServerObject(port, address);
                listenThread = new Thread(new ThreadStart(server.Listen));
                listenThread.Start(); //старт потока
            }
            catch (Exception ex)
            {
                server.Disconnect();
                Console.WriteLine(ex.Message);
            }
        }

        static void ReadAllSettings()
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;

                address = appSettings["addres"];
                if (!int.TryParse(appSettings["port"], out port))
                {
                    port = 8888;
                    Console.WriteLine("Порт задан не как целое число - преобразование невозможно.");
                    Console.WriteLine("Использю порт по умолчанию.");
                }
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Ошибка чтения настроек");
                Console.WriteLine("Использую адрес и порт по умолчанию.");
            }
        }
    }
}