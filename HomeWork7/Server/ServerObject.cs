﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading;
using СommonLibrary;

namespace Server
{
    public class ServerObject
    {
        int port = 8888;
        IPAddress ipAddress;

        public ServerObject(int port, string address)
        {
            this.port = port;
            if (!IPAddress.TryParse(address, out ipAddress))
                ipAddress = IPAddress.Any;
        }

        /// <summary>
        /// Сервер для прослушивания
        /// </summary>
        static TcpListener tcpListener;

        /// <summary>
        /// Список клиентов
        /// </summary>
        List<ClientObject> clients = new List<ClientObject>();

        /// <summary>
        /// Добавление нового клиента
        /// </summary>
        /// <param name="clientObject"></param>
        protected internal void AddConnection(ClientObject clientObject)
        {
            clients.Add(clientObject);
        }

        /// <summary>
        /// Удаление клиента
        /// </summary>
        /// <param name="id">Id клинета</param>
        protected internal void RemoveConnection(string id)
        {
            ClientObject client = clients.FirstOrDefault(c => c.Id == id);
            if (client != null)
                clients.Remove(client);
        }

        /// <summary>
        /// Прослушивание входящих подключений
        /// </summary>
        protected internal void Listen()
        {
            try
            {
                tcpListener = new TcpListener(ipAddress, port);
                tcpListener.Start();
                Console.WriteLine("Сервер запущен. Ожидание подключений...");

                while (true)
                {
                    TcpClient tcpClient = tcpListener.AcceptTcpClient();

                    ClientObject clientObject = new ClientObject(tcpClient, this);
                    Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
                    clientThread.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Disconnect();
            }
        }

        /// <summary>
        /// Проброска сообщения всем клиентам
        /// </summary>
        /// <param name="message"></param>
        /// <param name="id"></param>
        protected internal void BroadcastMessage(Message message, string id)
        {
            byte[] data = Encoding.Unicode.GetBytes(message.GetSendString());
            for (int i = 0; i < clients.Count; i++)
            {
                if (clients[i].Id != id) // если id клиента не равно id отправляющего
                {
                    clients[i].Stream.Write(data, 0, data.Length); //передача данных
                }
            }
        }

        /// <summary>
        /// Остановка работы сервера
        /// </summary>
        protected internal void Disconnect()
        {
            tcpListener.Stop();
            for (int i = 0; i < clients.Count; i++)
            {
                clients[i].Close();
            }
            Environment.Exit(0);
        }
    }
}