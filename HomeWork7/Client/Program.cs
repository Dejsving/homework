﻿using System;
using System.Configuration;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using СommonLibrary;

namespace Client
{
    class Program
    {
        static Random rnd = new Random();
        static int port = 8888;
        static string address = "127.0.0.1";

        static string userName;
        static TcpClient client;
        static NetworkStream stream;

        static void Main(string[] args)
        {
            Console.Write("Введите свое имя: ");
            userName = Console.ReadLine();
            client = new TcpClient();

            try
            {
                client.Connect(address, port);
                stream = client.GetStream();

                Message message = new Message(userName, "", rnd);
                byte[] data = Encoding.Unicode.GetBytes(message.GetSendString());
                stream.Write(data, 0, data.Length);

                Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
                receiveThread.Start();
                Console.WriteLine($"Добро пожаловать, {userName}");
                SendMessage();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
        }

        /// <summary>
        /// Отправка сообщения
        /// </summary>
        static void SendMessage()
        {
            Console.WriteLine("Введите сообщение: ");

            while (true)
            {
                Message message = new Message(userName, Console.ReadLine(), rnd);
                byte[] data = Encoding.Unicode.GetBytes(message.GetSendString());
                stream.Write(data, 0, data.Length);
            }
        }

        /// <summary>
        /// Получение сообщений от сервера
        /// </summary>
        static void ReceiveMessage()
        {
            while (true)
            {
                try
                {
                    byte[] data = new byte[2048]; // буфер для получаемых данных
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);

                    Message message = Message.GetSendMessage(builder.ToString());
                    Console.WriteLine(message);//вывод сообщения
                }
                catch
                {
                    Console.WriteLine("Подключение прервано!");
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        /// <summary>
        /// Отключение
        /// </summary>
        static void Disconnect()
        {
            if (stream != null)
                stream.Close();
            if (client != null)
                client.Close();
            Environment.Exit(0);
        }

        /// <summary>
        /// Чтение настроек
        /// </summary>
        static void ReadAllSettings()
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;

                address = appSettings["addres"];
                if (!int.TryParse(appSettings["port"], out port))
                {
                    port = 8888;
                    Console.WriteLine("Порт задан не как целое число - преобразование невозможно.");
                    Console.WriteLine("Использю порт по умолчанию.");
                }
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Ошибка чтения настроек");
                Console.WriteLine("Использю адрес и порт по умолчанию.");
            }
        }
    }
}