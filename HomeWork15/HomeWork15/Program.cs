﻿using System;
using System.Diagnostics;
using System.Text;

namespace HomeWork15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(CheckParallelSum(1_000));
            Console.WriteLine(CheckParallelSum(1_000_000));
            Console.WriteLine(CheckParallelSum(1_000_000_000));
            Console.ReadKey();
        }

        private static string CheckParallelSum(int size)
        {
            var sb = new StringBuilder();
            sb.Append($"Массив на {size} элементов");
            sb.AppendLine();
            var sumArray = new SumArray(size);
            var sw = new Stopwatch();

            sb.Append("Линейные вычисления:\t\t");
            sw.Start();
            sumArray.LinearSum();
            sw.Stop();
            sb.Append(sw.ElapsedMilliseconds);
            sb.AppendLine();

            sb.Append("Параллельные вычисления:\t");
            sw.Restart();
            sumArray.ParallelSum();
            sw.Stop();
            sb.Append(sw.ElapsedMilliseconds);
            sb.AppendLine();

            sb.Append("Параллельный LINQ:\t\t");
            sw.Restart();
            sumArray.LinqParallelSum();
            sw.Stop();
            sb.Append(sw.ElapsedMilliseconds);
            sb.AppendLine();

            return sb.ToString();
        }
    }
}
