﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork15
{
    public class SumArray
    {
        public int[] Array { get; }

        private readonly int _size;

        /// <summary>
        /// Класс для тестирование параллельных вычислений
        /// </summary>
        /// <param name="size">Должно быть кратно 10</param>
        public SumArray(int size)
        {
            _size = size;

            Array = new int[_size];
            for (int i = 0; i < _size; i++)
            {
                Array[i] = 1;
            }
        }

        public int LinearSum()
        {
            var result = 0;

            foreach (var i in Array)
                result += i;

            return result;
        }

        public int ParallelSum()
        {
            var pc = Environment.ProcessorCount;
            int step = _size / pc;
            int remDiv = _size - pc * step;

            var result = 0;

            int[] sums = new int[pc];
            for (int i = 0; i < pc; i++)
                sums[i] = 0;

            Parallel.For(0, pc, i =>
            {
                for (int j = 0; j < step; j++)
                {
                    sums[i] += Array[j + step * i];
                }
            });

            foreach (var sum in sums)
            {
                result += sum;
            }

            for (int i = 0; i < remDiv; i++)
            {
                result += Array[_size - i - 1];
            }

            return result;
        }

        public int LinqParallelSum()
        {
            return Array.AsParallel().Sum();
        }
    }
}