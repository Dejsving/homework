﻿using DataBaseInterface.CoreInterfaces;

namespace ADONetConnection.Core
{
    public class Teacher : ITeacher
    {
        private static int _lastId = 1;

        public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public int CourseId { get; set; }
        public ICourse Course { get; set; }

        public Teacher(string surname, string name, ICourse course)
        {
            Id = _lastId;
            Surname = surname;
            Name = name;
            Course = course;
            CourseId = Course.Id;
            _lastId++;
        }

        public Teacher()
        {
            
        }
    }
}