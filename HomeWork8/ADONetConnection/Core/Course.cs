﻿using DataBaseInterface.CoreInterfaces;

namespace ADONetConnection.Core
{
    public class Course : ICourse
    {
        private static int _lastId = 1;

        public int Id { get; set; }
        public string CourseName { get; set; }

        public Course(string courseName)
        {
            Id = _lastId;
            CourseName = courseName;
            _lastId++;
        }

        public Course()
        {
        }
    }
}