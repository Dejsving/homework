﻿using DataBaseInterface.CoreInterfaces;

namespace ADONetConnection.Core
{
    public class Homework : IHomework
    {
        private static int _lastId = 1;

        public int Id { get; set; }
        public int LessonId { get; set; }
        public ILesson Lesson { get; set; }

        public Homework(ILesson lesson)
        {
            Id = _lastId;
            Lesson = lesson;
            LessonId = Lesson.Id;
            _lastId++;
        }

        public Homework()
        {
            
        }
    }
}