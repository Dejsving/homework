﻿using DataBaseInterface.CoreInterfaces;

namespace ADONetConnection.Core
{
    public class Lesson : ILesson
    {
        private static int _lastId = 1;

        public int Id { get; set; }
        public int CourseId { get; set; }
        public ICourse Course { get; set; }
        public string Topic { get; set; }

        public Lesson(ICourse course, string topic)
        {
            Id = _lastId;
            Topic = topic;
            Course = course;
            CourseId = Course.Id;
            _lastId++;
        }

        public Lesson()
        {
            
        }
    }
}