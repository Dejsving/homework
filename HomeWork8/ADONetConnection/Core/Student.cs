﻿using DataBaseInterface.CoreInterfaces;

namespace ADONetConnection.Core
{
    public class Student : IStudent
    {
        private static int _lastId = 1;

        public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public int CourseId { get; set; }
        public ICourse Course { get; set; }
        public bool IsPaid { get; set; }

        public Student(string surname, string name, ICourse course, bool isPaid)
        {
            Id = _lastId;
            Surname = surname;
            Name = name;
            Course = course;
            CourseId = Course.Id;
            IsPaid = isPaid;
            _lastId++;
        }

        public Student()
        {
            
        }
    }
}