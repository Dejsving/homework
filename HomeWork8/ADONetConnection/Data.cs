﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ADONetConnection.Core;
using DataBaseInterface;
using DataBaseInterface.CoreInterfaces;

namespace ADONetConnection
{
    public class Data : IData
    {
        private readonly SqlConnection _connection;

        public List<ICourse> Courses { get; private set; }
        public List<IHomework> Homeworks { get; private set; }
        public List<ILesson> Lessons { get; private set; }
        public List<IStudent> Students { get; private set; }
        public List<ITeacher> Teachers { get; private set; }

        public Data(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
            _connection.Open();
            // заполняем внутренние списки
            FillCourses();
            FillHomeworks();
            FillLessons();
            FillStudents();
            FillTeachers();
        }

        #region CtorAdditions
        private void FillCourses()
        {
            Courses = new List<ICourse>();
            string sqlExpression = "SELECT Id, CourseName FROM otus.Courses";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            var resultCommand = command.ExecuteReader();
            if (resultCommand.HasRows)
            {
                while (resultCommand.Read())
                {
                    Courses.Add(new Course
                    {
                        Id = (int) resultCommand.GetValue(0),
                        CourseName = (string) resultCommand.GetValue(1)
                    });
                }
            }
            resultCommand.Close();
        }

        private void FillHomeworks()
        {
            Homeworks = new List<IHomework>();
            string sqlExpression = "SELECT Id, LessonId FROM otus.Homeworks";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            var resultCommand = command.ExecuteReader();
            if (resultCommand.HasRows)
            {
                while (resultCommand.Read())
                {
                    Homeworks.Add(new Homework
                    {
                        Id = (int)resultCommand.GetValue(0),
                        LessonId = (int)resultCommand.GetValue(1)
                    });
                }
            }
            resultCommand.Close();
        }

        private void FillLessons()
        {
            Lessons = new List<ILesson>();
            string sqlExpression = "SELECT Id, CourseId FROM otus.Lessons";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            var resultCommand = command.ExecuteReader();
            if (resultCommand.HasRows)
            {
                while (resultCommand.Read())
                {
                    Lessons.Add(new Lesson
                    {
                        Id = (int)resultCommand.GetValue(0),
                        CourseId = (int)resultCommand.GetValue(1)
                    });
                }
            }
            resultCommand.Close();
        }

        private void FillStudents()
        {
            Students = new List<IStudent>();
            string sqlExpression = "SELECT Id, CourseId, Name, Surname, IsPaid FROM otus.Students";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            var resultCommand = command.ExecuteReader();
            if (resultCommand.HasRows)
            {
                while (resultCommand.Read())
                {
                    Students.Add(new Student
                    {
                        Id = (int)resultCommand.GetValue(0),
                        CourseId = (int)resultCommand.GetValue(1),
                        Name = (string)resultCommand.GetValue(2),
                        Surname = (string)resultCommand.GetValue(3),
                        IsPaid = (bool)resultCommand.GetValue(4)
                    });
                }
            }
            resultCommand.Close();
        }

        private void FillTeachers()
        {
            Teachers = new List<ITeacher>();
            string sqlExpression = "SELECT Id, CourseId, Name, Surname FROM otus.Teachers";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            var resultCommand = command.ExecuteReader();
            if (resultCommand.HasRows)
            {
                while (resultCommand.Read())
                {
                    Teachers.Add(new Teacher
                    {
                        Id = (int)resultCommand.GetValue(0),
                        CourseId = (int)resultCommand.GetValue(1),
                        Name = (string)resultCommand.GetValue(2),
                        Surname = (string)resultCommand.GetValue(3)
                    });
                }
            }
            resultCommand.Close();
        }
        #endregion
        
        public void Dispose()
        {
            Save();
            _connection.Close();
        }

        public void Save()
        {
            var count = SaveCourses();
#if DEBUG
            Console.WriteLine($"Courses: Обновлено строк: {count}");
#endif

            count = SaveHomeworks();
#if DEBUG
            Console.WriteLine($"Homeworks: Обновлено строк: {count}");
#endif

            count = SaveLessons();
#if DEBUG
            Console.WriteLine($"Lessons: Обновлено строк: {count}");
#endif

            count = SaveStudents();
#if DEBUG
            Console.WriteLine($"Students: Обновлено строк: {count}");
#endif

            count = SaveTeachers();
#if DEBUG
            Console.WriteLine($"Teachers: Обновлено строк: {count}");
#endif
        }

        #region Save
        private int SaveCourses()
        {
            var count = 0;
            foreach (var course in Courses)
            {
                string sqlExpression =
                    $"update otus.Courses set CourseName = '{course.CourseName}' where Id = {course.Id}";
                SqlCommand command = new SqlCommand(sqlExpression, _connection);
                int number = command.ExecuteNonQuery();
                count += number;
            }
            return count;
        }

        private int SaveHomeworks()
        {
            var count = 0;
            foreach (var homework in Homeworks)
            {
                string sqlExpression =
                    $"update otus.Homeworks set LessonId = {homework.LessonId} where Id = {homework.Id}";
                SqlCommand command = new SqlCommand(sqlExpression, _connection);
                int number = command.ExecuteNonQuery();
                count += number;
            }
            return count;
        }

        private int SaveLessons()
        {
            var count = 0;
            foreach (var lesson in Lessons)
            {
                string sqlExpression =
                    $"update otus.Lessons set CourseId = {lesson.CourseId}, Topic = '{lesson.Topic}' where Id = {lesson.Id}";
                SqlCommand command = new SqlCommand(sqlExpression, _connection);
                int number = command.ExecuteNonQuery();
                count += number;
            }
            return count;
        }

        private int SaveStudents()
        {
            var count = 0;
            foreach (var student in Students)
            {
                string sqlExpression =
                    $"update otus.Students set Name = '{student.Name}', Surname = '{student.Surname}', IsPaid = {(student.IsPaid ? 1 : 0)}, CourseId = {student.CourseId} where Id = {student.Id}";
                SqlCommand command = new SqlCommand(sqlExpression, _connection);
                int number = command.ExecuteNonQuery();
                count += number;
            }
            return count;
        }

        private int SaveTeachers()
        {
            var count = 0;
            foreach (var teacher in Teachers)
            {
                string sqlExpression =
                    $"update otus.Teachers set Name = '{teacher.Name}', Surname = '{teacher.Surname}', CourseId = {teacher.CourseId} where Id = {teacher.Id}";
                SqlCommand command = new SqlCommand(sqlExpression, _connection);
                int number = command.ExecuteNonQuery();
                count += number;
            }
            return count;
        }
        #endregion
        #region Add
        public void AddCourse(ICourse value)
        {
            Courses.Add(value);
            string sqlExpression = $"INSERT INTO otus.Courses (CourseName) VALUES ('{value.CourseName}')";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Добавлено строк: {number}");
#endif
        }

        public void AddCourses(IEnumerable<ICourse> values)
        {
            Courses.AddRange(values);
            StringBuilder sb = new StringBuilder("INSERT INTO otus.Courses (CourseName) VALUES ");
            foreach (var course in values)
            {
                sb.Append($"('{course.CourseName}'),");
            }
            sb.Remove(sb.Length - 1, 1);
            string sqlExpression = sb.ToString();
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Добавлено строк: {number}");
#endif
        }

        public void AddHomework(IHomework value)
        {
            Homeworks.Add(value);
            string sqlExpression = $"INSERT INTO otus.Homeworks (LessonId) VALUES ({value.LessonId})";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Добавлено строк: {number}");
#endif
        }

        public void AddHomeworks(IEnumerable<IHomework> values)
        {
            Homeworks.AddRange(values);
            StringBuilder sb = new StringBuilder("INSERT INTO otus.Homeworks (LessonId) VALUES ");
            foreach (var homework in values)
            {
                sb.Append($"({homework.LessonId}),");
            }
            sb.Remove(sb.Length - 1, 1); 
            string sqlExpression = sb.ToString();
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Добавлено строк: {number}");
#endif
        }

        public void AddLesson(ILesson value)
        {
            Lessons.Add(value);
            string sqlExpression = $"INSERT INTO otus.Lessons (CourseId, Topic) VALUES ({value.CourseId}, '{value.Topic}')";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Добавлено строк: {number}");
#endif
        }

        public void AddLessons(IEnumerable<ILesson> values)
        {
            Lessons.AddRange(values);
            StringBuilder sb = new StringBuilder("INSERT INTO otus.Lessons (CourseId, Topic) VALUES ");
            foreach (var lesson in values)
            {
                sb.Append($"({lesson.CourseId}, '{lesson.Topic}'),");
            }
            sb.Remove(sb.Length - 1, 1);
            string sqlExpression = sb.ToString();
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Добавлено строк: {number}");
#endif
        }

        public void AddStudent(IStudent value)
        {
            Students.Add(value);
            string sqlExpression =
                $"INSERT INTO otus.Students (Name, Surname, IsPaid, CourseId) VALUES ('{value.Name}', '{value.Surname}', {(value.IsPaid ? 1 : 0)}, {value.CourseId})";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Добавлено строк: {number}");
#endif
        }

        public void AddStudents(IEnumerable<IStudent> values)
        {
            Students.AddRange(values);
            StringBuilder sb = new StringBuilder("INSERT INTO otus.Students (Name, Surname, IsPaid, CourseId) VALUES ");
            foreach (var student in values)
            {
                sb.Append($"('{student.Name}', '{student.Surname}', {(student.IsPaid ? 1 : 0)}, {student.CourseId}),");
            }
            sb.Remove(sb.Length - 1, 1);
            string sqlExpression = sb.ToString();
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Добавлено строк: {number}");
#endif
        }

        public void AddTeacher(ITeacher value)
        {
            Teachers.Add(value);
            string sqlExpression =
                $"INSERT INTO otus.Teachers (Name, Surname, CourseId) VALUES ('{value.Name}', '{value.Surname}', {value.CourseId})";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Добавлено строк: {number}");
#endif
        }

        public void AddTeachers(IEnumerable<ITeacher> values)
        {
            Teachers.AddRange(values);
            StringBuilder sb = new StringBuilder("INSERT INTO otus.Teachers (Name, Surname, CourseId) VALUES ");
            foreach (var teacher in values)
            {
                sb.Append($"('{teacher.Name}', '{teacher.Surname}', {teacher.CourseId}),");
            }
            sb.Remove(sb.Length - 1, 1);
            string sqlExpression = sb.ToString();
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Добавлено строк: {number}");
#endif
        }
        #endregion
        #region Remove
        public void RemoveCourse(ICourse value)
        {
            Courses.Remove(value);
            string sqlExpression = $"DELETE FROM otus.Courses WHERE Id = {value.Id}";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Удалено строк: {number}");
#endif
        }

        public void RemoveCourses(IEnumerable<ICourse> values)
        {
            var removeIds = values.Select(t => t.Id);
            Courses.RemoveAll(t => removeIds.Contains(t.Id));
            var removeIdsString = string.Join(",",removeIds);
                              string sqlExpression = $"DELETE FROM otus.Courses WHERE Id in ({removeIdsString}))";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Удалено строк: {number}");
#endif
        }

        public void RemoveHomework(IHomework value)
        {
            Homeworks.Remove(value);
            string sqlExpression = $"DELETE FROM otus.Homeworks WHERE Id = {value.Id})";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Удалено строк: {number}");
#endif
        }

        public void RemoveHomeworks(IEnumerable<IHomework> values)
        {
            var removeIds = values.Select(t => t.Id);
            Homeworks.RemoveAll(t => removeIds.Contains(t.Id));
            var removeIdsString = string.Join(",", removeIds);
            string sqlExpression = $"DELETE FROM otus.Homeworks WHERE Id in ({removeIdsString}))";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Удалено строк: {number}");
#endif
        }

        public void RemoveLesson(ILesson value)
        {
            Lessons.Remove(value);
            string sqlExpression = $"DELETE FROM otus.Lessons WHERE Id = {value.Id})";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Удалено строк: {number}");
#endif
        }

        public void RemoveLessons(IEnumerable<ILesson> values)
        {
            var removeIds = values.Select(t => t.Id);
            Lessons.RemoveAll(t => removeIds.Contains(t.Id));
            var removeIdsString = string.Join(",", removeIds);
            string sqlExpression = $"DELETE FROM otus.Lessons WHERE Id in ({removeIdsString})";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Удалено строк: {number}");
#endif
        }

        public void RemoveStudent(IStudent value)
        {
            Students.Remove(value);
            string sqlExpression = $"DELETE FROM otus.Students WHERE Id = {value.Id})";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Удалено строк: {number}");
#endif
        }

        public void RemoveStudents(IEnumerable<IStudent> values)
        {
            var removeIds = values.Select(t => t.Id);
            Students.RemoveAll(t => removeIds.Contains(t.Id));
            var removeIdsString = string.Join(",", removeIds);
            string sqlExpression = $"DELETE FROM otus.Students WHERE Id in ({removeIdsString}))";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Удалено строк: {number}");
#endif
        }

        public void RemoveTeacher(ITeacher value)
        {
            Teachers.Remove(value);
            string sqlExpression = $"DELETE FROM otus.Teachers WHERE Id = {value.Id})";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Удалено строк: {number}");
#endif
        }

        public void RemoveTeachers(IEnumerable<ITeacher> values)
        {
            var removeIds = values.Select(t => t.Id);
            Teachers.RemoveAll(t => removeIds.Contains(t.Id));
            var removeIdsString = string.Join(",", removeIds);
            string sqlExpression = $"DELETE FROM otus.Teachers WHERE Id in ({removeIdsString}))";
            SqlCommand command = new SqlCommand(sqlExpression, _connection);
            int number = command.ExecuteNonQuery();
#if DEBUG
            Console.WriteLine($"Удалено строк: {number}");
#endif
        }
        #endregion
    }
}