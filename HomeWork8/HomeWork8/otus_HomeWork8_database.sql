-- ����������������� ���� �� ��� ����
use OTUS_HomeWork8;
alter database OTUS_HomeWork8 set single_user with rollback immediate
use master
drop database if exists OTUS_HomeWork8;
GO

create database OTUS_HomeWork8;
GO

use OTUS_HomeWork8;
GO

drop table if exists otus.Homeworks
drop table if exists otus.Lessons
drop table if exists otus.Teachers
drop table if exists otus.Students
drop table if exists otus.Courses
drop schema if exists otus
GO

create schema otus;
GO

create table otus.Courses
(
	Id int identity(1,1) not null primary key,
	CourseName nvarchar(50) not null
);
GO

create table otus.Students
(
    Id int identity(1,1) not null primary key,
    Surname nvarchar(50) not null,
    Name nvarchar(50) not null,
	IsPaid bit not null,
    CourseId int not null constraint FK_Students_To_Courses foreign key (CourseId) references otus.Courses (Id) on delete cascade
);
GO

create table otus.Teachers
(
    Id int identity(1,1) not null primary key,
    Surname nvarchar(50) not null,
    Name nvarchar(50) not null,
    CourseId int not null constraint FK_Teachers_To_Courses foreign key (CourseId) references otus.Courses (Id) on delete cascade
);
GO

create table otus.Lessons
(
	Id int identity(1,1) not null primary key,
	CourseId int not null constraint FK_Lessons_To_Courses foreign key (CourseId) references otus.Courses (Id) on delete cascade,
	Topic nvarchar(50) not null
);
GO

create table otus.Homeworks
(
	Id int identity(1,1) not null primary key,
	LessonId int not null constraint FK_Homeworks_To_Lessons foreign key (LessonId) references otus.Lessons (Id) on delete cascade
);
GO


