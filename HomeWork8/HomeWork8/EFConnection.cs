﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityFrameworkConnection;
using EntityFrameworkConnection.Core;

namespace HomeWork8
{
    public static class EFConnection
    {
        public static void CheckConnection(string connectionString)
        {
            var course = new Course("C# Language");
            var courses = new List<Course>
            {
                new Course("C++ Language"),
                new Course("C Language")
            };

            var lesson = new Lesson(course, "Linq - плюсы и минусы");
            var lessons = new List<Lesson>
            {
                new Lesson(courses[0], "Основы ООП"),
                new Lesson(courses[1], "Основы языка")
            };

            var homework = new Homework(lesson);
            var homeworks = new List<Homework>
            {
                new Homework(lessons[0]),
                new Homework(lessons[1])
            };

            var student = new Student("Сергей", "Колесников", course, true);
            var students = new List<Student>
            {
                new Student("Илья", "Лебедев", courses[0], true),
                new Student("Виктор", "Захаров", courses[1], false)
            };

            var teacher = new Teacher("Андрей", "Орлов", course);
            var teachers = new List<Teacher>
            {
                new Teacher("Виталий", "Борисов", courses[0]),
                new Teacher("Илья", "Андреев", courses[0])
            };

            using (var data = new Data(connectionString))
            {
                data.AddCourse(course);
                data.AddCourses(courses);
                data.AddLesson(lesson);
                data.AddLessons(lessons);
                data.AddStudent(student);
                data.AddStudents(students);
                data.AddTeacher(teacher);
                data.AddTeachers(teachers);
                data.AddHomework(homework);
                data.AddHomeworks(homeworks);
                Console.WriteLine("Добавления успешны");
                var fCourse = data.Courses.Find(t => t.Id == 2);
                fCourse.CourseName = "Python Language";
                data.Save();
                var fStudent = data.Students.Find(t => t.Id == 2);
                fStudent.Name = "Николай";
                fStudent.Surname = "Степанов";
                data.Save();
                fStudent = data.Students.Find(t => !t.IsPaid);
                fStudent.IsPaid = true;
                data.Save();
                var fTeacher = data.Teachers.Find(t => t.Id == 2);
                fTeacher.Name = "Георгий";
                fTeacher.Surname = "Лебедев";
                data.Save();
                Console.WriteLine("Изменения успешны");
                data.RemoveCourse(data.Courses.Find(t => t.Id == 2));
                data.RemoveLessons(data.Lessons.Where(t => t.Id > 1).ToList());
                Console.WriteLine("Удаления успешны");
            }
        }
    }
}