﻿using System;
using System.Configuration;

namespace HomeWork8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start");
            
            var connectionType = GetConnectionType();
            var connectionString = GetConnectionString();
            if (connectionType == ConnectionType.ADONet)
            {
                ADOConnection.CheckConnection(connectionString);
            }
            else if (connectionType == ConnectionType.EntityFramework)
            {
                EFConnection.CheckConnection(connectionString);
            }
            else
            {
                Console.WriteLine("Неверный тип подключения");
            }

            Console.WriteLine("Finish");
            Console.ReadKey();
        }

        static ConnectionType GetConnectionType()
        {
            var appSettings = ConfigurationManager.AppSettings;
            var connectionTypeString = appSettings["connectionType"];
            if (Enum.TryParse(connectionTypeString, out ConnectionType connectionType))
            {
                return connectionType;
            }
            return ConnectionType.EntityFramework;
        }

        static string GetConnectionString()
        {
            var appSettings = ConfigurationManager.AppSettings;
            var connectionString = appSettings["connectionString"];
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException();
            return connectionString;
        }
    }
}