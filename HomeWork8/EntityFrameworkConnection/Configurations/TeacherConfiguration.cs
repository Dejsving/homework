﻿using System.Data.Entity.ModelConfiguration;
using EntityFrameworkConnection.Core;

namespace EntityFrameworkConnection.Configurations
{
    public class TeacherConfiguration : EntityTypeConfiguration<Teacher>
    {
        public TeacherConfiguration()
        {
            ToTable("Teachers", "otus");
            HasKey(t => t.Id);
            HasRequired(t => t.Course).WithMany().HasForeignKey(t => t.CourseId);
        }
    }
}