﻿using System.Data.Entity.ModelConfiguration;
using EntityFrameworkConnection.Core;

namespace EntityFrameworkConnection.Configurations
{
    public class CourseConfiguration : EntityTypeConfiguration<Course>
    {
        public CourseConfiguration()
        {
            ToTable("Courses", "otus");
            HasKey(t => t.Id);
        }
    }
}