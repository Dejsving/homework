﻿using System.Data.Entity.ModelConfiguration;
using EntityFrameworkConnection.Core;

namespace EntityFrameworkConnection.Configurations
{
    public class HomeworkConfiguration : EntityTypeConfiguration<Homework>
    {
        public HomeworkConfiguration()
        {
            ToTable("Homeworks", "otus");
            HasKey(t => t.Id);
            HasRequired(t => t.Lesson).WithMany().HasForeignKey(t => t.LessonId);
        }
    }
}