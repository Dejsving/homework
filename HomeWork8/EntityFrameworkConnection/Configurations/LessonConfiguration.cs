﻿using System.Data.Entity.ModelConfiguration;
using EntityFrameworkConnection.Core;

namespace EntityFrameworkConnection.Configurations
{
    public class LessonConfiguration : EntityTypeConfiguration<Lesson>
    {
        public LessonConfiguration()
        {
            ToTable("Lessons", "otus");
            HasKey(t => t.Id);
            HasRequired(t => t.Course).WithMany().HasForeignKey(t => t.CourseId);
        }
    }
}