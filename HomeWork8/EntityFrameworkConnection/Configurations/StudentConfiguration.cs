﻿using System.Data.Entity.ModelConfiguration;
using EntityFrameworkConnection.Core;

namespace EntityFrameworkConnection.Configurations
{
    public class StudentConfiguration : EntityTypeConfiguration<Student>
    {
        public StudentConfiguration()
        {
            ToTable("Students", "otus");
            HasKey(t => t.Id);
            HasRequired(t => t.Course).WithMany().HasForeignKey(t => t.CourseId);
        }
    }
}