﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DataBaseInterface.CoreInterfaces;

namespace EntityFrameworkConnection.Core
{
    [Table("Lessons", Schema = "otus")]
    public class Lesson : ILesson
    {
        public int Id { get; set; }
        public int CourseId { get; set; }
        public virtual ICourse Course { get; set; }
        public string Topic { get; set; }

        public Lesson(ICourse course, string topic)
        {
            Topic = topic;
            Course = course;
            CourseId = course.Id;
        }
    }
}