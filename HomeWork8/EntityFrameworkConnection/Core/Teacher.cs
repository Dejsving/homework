﻿using System.ComponentModel.DataAnnotations.Schema;
using DataBaseInterface.CoreInterfaces;

namespace EntityFrameworkConnection.Core
{
    [Table("Teachers", Schema = "otus")]
    public class Teacher : ITeacher
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public int CourseId { get; set; }
        public virtual ICourse Course { get; set; }

        public Teacher(string surname, string name, ICourse course)
        {
            Surname = surname;
            Name = name;
            Course = course;
            CourseId = course.Id;
        }
    }
}