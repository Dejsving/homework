﻿using System.ComponentModel.DataAnnotations.Schema;
using DataBaseInterface.CoreInterfaces;

namespace EntityFrameworkConnection.Core
{
    [Table("Homeworks", Schema = "otus")]
    public class Homework : IHomework
    {
        public int Id { get; set; }
        public int LessonId { get; set; }
        public virtual ILesson Lesson { get; set; }

        public Homework(ILesson lesson)
        {
            Lesson = lesson;
            LessonId = lesson.Id;
        }
    }
}