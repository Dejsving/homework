﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DataBaseInterface.CoreInterfaces;

namespace EntityFrameworkConnection.Core
{
    [Table("Courses", Schema = "otus")]
    public class Course : ICourse
    {
        public int Id { get; set; }
        public string CourseName { get; set; }
        public int TeacherId { get; set; }

        public Course(string courseName)
        {
            CourseName = courseName;
        }
    }
}