﻿using System.ComponentModel.DataAnnotations.Schema;
using DataBaseInterface.CoreInterfaces;

namespace EntityFrameworkConnection.Core
{
    [Table("Students", Schema = "otus")]
    public class Student : IStudent
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public int CourseId { get; set; }
        public virtual ICourse Course { get; set; }
        public bool IsPaid { get; set; }

        public Student(string surname, string name, ICourse course, bool isPaid)
        {
            Surname = surname;
            Name = name;
            Course = course;
            CourseId = course.Id;
            IsPaid = isPaid;
        }
    }
}