﻿using System.Collections.Generic;
using System.Linq;
using DataBaseInterface;
using DataBaseInterface.CoreInterfaces;
using EntityFrameworkConnection.Core;

namespace EntityFrameworkConnection
{
    public class Data : IData
    {
        private OtusContext Context { get; }

        public List<ICourse> Courses { get; }
        public List<IHomework> Homeworks { get; }
        public List<ILesson> Lessons { get; }
        public List<IStudent> Students { get; }
        public List<ITeacher> Teachers { get; }
        public Data(string connectionString)
        {
            Context = new OtusContext(connectionString);
            Courses = new List<ICourse>(Context.Courses);
            Homeworks = new List<IHomework>(Context.Homeworks);
            Lessons = new List<ILesson>(Context.Lessons);
            Students = new List<IStudent>(Context.Students);
            Teachers = new List<ITeacher>(Context.Teachers);
        }

        public void Save()
        {
            Context.SaveChanges();
        }
        #region Add
        public void AddCourse(ICourse value)
        {
            Courses.Add(value);
            Context.Courses.Add((Course)value);
            Context.SaveChangesAsync();
        }
        public void AddCourses(IEnumerable<ICourse> values)
        {
            Courses.AddRange(values);
            Context.Courses.AddRange(values.Cast<Course>());
            Context.SaveChangesAsync();
        }
        public void AddHomework(IHomework value)
        {
            Homeworks.Add(value);
            Context.Homeworks.Add((Homework)value);
            Context.SaveChangesAsync();
        }
        public void AddHomeworks(IEnumerable<IHomework> values)
        {
            Context.Homeworks.AddRange(values.Cast<Homework>());
            Context.SaveChangesAsync();
        }
        public void AddLesson(ILesson value)
        {
            Lessons.Add(value);
            Context.Lessons.Add((Lesson)value);
            Context.SaveChangesAsync();
        }
        public void AddLessons(IEnumerable<ILesson> values)
        {
            Context.Lessons.AddRange(values.Cast<Lesson>());
            Context.SaveChangesAsync();
        }
        public void AddStudent(IStudent value)
        {
            Students.Add(value);
            Context.Students.Add((Student)value);
            Context.SaveChangesAsync();
        }
        public void AddStudents(IEnumerable<IStudent> values)
        {
            Context.Students.AddRange(values.Cast<Student>());
            Context.SaveChangesAsync();
        }
        public void AddTeacher(ITeacher value)
        {
            Teachers.Add(value);
            Context.Teachers.Add((Teacher)value);
            Context.SaveChangesAsync();
        }
        public void AddTeachers(IEnumerable<ITeacher> values)
        {
            Context.Teachers.AddRange(values.Cast<Teacher>());
            Context.SaveChangesAsync();
        }
        #endregion
        #region Remove
        public void RemoveCourse(ICourse value)
        {
            Context.Courses.Remove((Course)value);
            Context.SaveChangesAsync();
        }
        public void RemoveCourses(IEnumerable<ICourse> values)
        {
            Context.Courses.RemoveRange(values.Cast<Course>());
            Context.SaveChangesAsync();
        }
        public void RemoveHomework(IHomework value)
        {
            Context.Homeworks.Remove((Homework)value);
            Context.SaveChangesAsync();
        }
        public void RemoveHomeworks(IEnumerable<IHomework> values)
        {
            Context.Homeworks.RemoveRange(values.Cast<Homework>());
            Context.SaveChangesAsync();
        }
        public void RemoveLesson(ILesson value)
        {
            Context.Lessons.Remove((Lesson)value);
            Context.SaveChangesAsync();
        }
        public void RemoveLessons(IEnumerable<ILesson> values)
        {
            Context.Lessons.RemoveRange(values.Cast<Lesson>());
            Context.SaveChangesAsync();
        }
        public void RemoveStudent(IStudent value)
        {
            Context.Students.Remove((Student)value);
            Context.SaveChangesAsync();
        }
        public void RemoveStudents(IEnumerable<IStudent> values)
        {
            Context.Students.RemoveRange(values.Cast<Student>());
            Context.SaveChangesAsync();
        }
        public void RemoveTeacher(ITeacher value)
        {
            Context.Teachers.Remove((Teacher)value);
            Context.SaveChangesAsync();
        }
        public void RemoveTeachers(IEnumerable<ITeacher> values)
        {
            Context.Teachers.RemoveRange(values.Cast<Teacher>());
            Context.SaveChangesAsync();
        }
        #endregion
        public void Dispose()
        {
            Context.SaveChanges();
            Context.Dispose();
        }
    }
}