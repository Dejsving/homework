﻿using System.Collections.Generic;
using DataBaseInterface.CoreInterfaces;

namespace DataBaseInterface.Repositories
{
    public interface ILessonRepository
    {
        void AddLesson(ILesson value);
        void AddLessons(IEnumerable<ILesson> values);

        void RemoveLesson(ILesson value);
        void RemoveLessons(IEnumerable<ILesson> values);
    }
}