﻿using System.Collections.Generic;
using DataBaseInterface.CoreInterfaces;

namespace DataBaseInterface.Repositories
{
    public interface IHomeworkRepository
    {
        void AddHomework(IHomework value);
        void AddHomeworks(IEnumerable<IHomework> values);

        void RemoveHomework(IHomework value);
        void RemoveHomeworks(IEnumerable<IHomework> values);
    }
}