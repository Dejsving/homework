﻿using System.Collections.Generic;
using DataBaseInterface.CoreInterfaces;

namespace DataBaseInterface.Repositories
{
    public interface IStudentRepository
    {
        void AddStudent(IStudent value);
        void AddStudents(IEnumerable<IStudent> values);

        void RemoveStudent(IStudent value);
        void RemoveStudents(IEnumerable<IStudent> values);
    }
}