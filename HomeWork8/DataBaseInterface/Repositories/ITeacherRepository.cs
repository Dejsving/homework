﻿using System.Collections.Generic;
using DataBaseInterface.CoreInterfaces;

namespace DataBaseInterface.Repositories
{
    public interface ITeacherRepository
    {
        void AddTeacher(ITeacher value);
        void AddTeachers(IEnumerable<ITeacher> values);

        void RemoveTeacher(ITeacher value);
        void RemoveTeachers(IEnumerable<ITeacher> values);
    }
}