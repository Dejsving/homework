﻿using System.Collections.Generic;
using DataBaseInterface.CoreInterfaces;

namespace DataBaseInterface.Repositories
{
    public interface ICourseRepository
    {
        void AddCourse(ICourse value);
        void AddCourses(IEnumerable<ICourse> values);

        void RemoveCourse(ICourse value);
        void RemoveCourses(IEnumerable<ICourse> values);
    }
}