﻿using System;
using DataBaseInterface.Repositories;

namespace DataBaseInterface
{
    public interface IData : ICourseRepository, IHomeworkRepository, ILessonRepository, IStudentRepository, ITeacherRepository, IDisposable
    {
        void Save();
    }
}