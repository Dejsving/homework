﻿namespace DataBaseInterface.CoreInterfaces
{
    public interface ICourse
    {
        int Id { get; set; }
        string CourseName { get; set; }
    }
}