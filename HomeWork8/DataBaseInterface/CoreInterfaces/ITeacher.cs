﻿namespace DataBaseInterface.CoreInterfaces
{
    public interface ITeacher
    {
        int Id { get; set; }
        string Surname { get; set; }
        string Name { get; set; }
        int CourseId { get; set; }
        ICourse Course { get; set; }
    }
}