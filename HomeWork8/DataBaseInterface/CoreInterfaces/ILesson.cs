﻿namespace DataBaseInterface.CoreInterfaces
{
    public interface ILesson
    {
        int Id { get; set; }
        int CourseId { get; set; }
        ICourse Course { get; set; }
        string Topic { get; set; }
    }
}