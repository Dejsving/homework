﻿namespace DataBaseInterface.CoreInterfaces
{
    public interface IHomework
    {
        int Id { get; set; }
        int LessonId { get; set; }
        ILesson Lesson { get; set; }
    }
}