﻿using HomeWork;
using System;

namespace ComplexNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            CheckComplex();
            Console.ReadKey();
        }

        static void CheckComplex()
        {
            Complex z1, z2, z3, z4, z5, z6, z7, z8, z9;
            Complex[] z; double t = 4.3; int n = 5;
            #region Инициализация
            Console.WriteLine("Инициализация");
            z1 = new Complex(); // нуль
            Console.WriteLine("z1 = {0}", z1);
            z2 = new Complex(1); // чисто вещественное число
            Console.WriteLine("z2 = {0}", z2);
            z3 = new Complex(2); // чисто вещественное число
            Console.WriteLine("z3 = {0}", z3);
            z4 = new Complex(0, 1); // чисто мнимиое число
            Console.WriteLine("z4 = {0}", z4);
            z5 = new Complex(0, -1); // чисто мнимиое число
            Console.WriteLine("z5 = {0}", z5);
            z6 = new Complex(1, -3); // комплексное число
            Console.WriteLine("z6 = {0}", z6);
            z7 = new Complex(-3, 1); // комплексное число
            Console.WriteLine("z7 = {0}", z7);
            z8 = Complex.Euler(8, Complex.pi / 2); // полярное задание
            Console.WriteLine("z8 = {0}", z8);
            z9 = Complex.Euler(1, -Complex.pi / 4); // полярное задание
            Console.WriteLine("z9 = {0}", z9);
            #endregion
            #region Функции
            z9 = z7 + z4;
            Console.WriteLine("z7 + z4 = {0}", z9); // сумма
            z8 = z6 - z5;
            Console.WriteLine("z6 - z5 = {0}", z8); // разность
            z2 = Complex.pow(z6, n); Console.WriteLine("z6^{1}={0}", z2, n); // возведение в степень
            t += (double)z6; Console.WriteLine("Было: 4,3. t = {0}", t); // явное преобразование типов
            z5 += t; Console.WriteLine("z5+4.3 = {0}", z5); // неявное преобразование типов
            Console.WriteLine("Re(z6)={0}, Im(z6)={1}, mod(z6)={2}, arg(z6)={3}", z6.re, z6.im, z6.mod, z6.arg);
            z6.re = 3; z6.im = -2;
            Console.WriteLine("Измененный z6:");
            Console.WriteLine("Re(z6)={0}, Im(z6)={1}, mod(z6)={2}, arg(z6)={3}", z6.re, z6.im, z6.mod, z6.arg);
            z9 = z7 * z4; Console.WriteLine("z7 * z4 = {0}", z9); // умножение
            z8 = z6 / z5; Console.WriteLine("z6 / z5 = {0}", z8); // деление
            t = 10;
            Console.WriteLine("z9 = {0}", z9);
            z9 *= 10; Console.WriteLine("z9*10 = {0}", z9); // умножение на число
            z9 /= 10; Console.WriteLine("z9*10/10 = {0}", z9); // деление на число
            Console.WriteLine("z4 = {0}", z4);
            z5 = Complex.Conjugate(z4);
            Console.WriteLine("(сопр)z4 = {0}", z5); // сопряженное число
            z = Complex.radical(z5, n);
            Console.WriteLine("Корни {0} степени:", n);
            for (int i = 0; i < n; i++) Console.WriteLine("z5[{0}]={1}", i, z[i]);
            z7.re = 5.36;
            z7.im = 3.25;
            z8.re = 5.36;
            z8.im = 3.25;
            Console.WriteLine("Хеш-функция: z7:{0}, z8:{1}, z5:{2}", z7.GetHashCode(), z8.GetHashCode(), z5.GetHashCode()); // Хэш-функция
            Console.WriteLine("{2}={1}::::{0}", z8.Equals(z7), z7, z8); // Сравнения
            Console.WriteLine("Равенство null == {1} = {0}", null == z5, z5);
            Console.WriteLine("Равенство {2} == {1} = {0}", z8 == z7, z7, z8); // ==
            Console.WriteLine("Равенство {2} != {1} = {0}", z4 != z5, z5, z4); // !=
            #endregion
            #region Индексатор
            Console.WriteLine("Тест индексатора");
            var z0 = new Complex(21, 15);
            Console.WriteLine($"z0={z0}");
            for (int i = 0; i < 2; i++)
            {
                Console.WriteLine(z0[i]);
            }
            for (int i = 0; i < 2; i++)
            {
                z0[i] = -7 * i + 5;
            }
            Console.WriteLine($"z0={z0}");
            #endregion
        }
    }
}
